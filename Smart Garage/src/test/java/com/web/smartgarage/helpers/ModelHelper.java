package com.web.smartgarage.helpers;

import com.web.smartgarage.models.Model;


public class ModelHelper {

    public static Model createMockModel() {
        var mockManufacturer= VehicleHelper.createMockManufacturer();
        var mockModel = new Model();
        mockModel.setId(1L);
        mockModel.setManufacturer(mockManufacturer);
        mockModel.setName("WRX STI");
        return mockModel;
    }

}
