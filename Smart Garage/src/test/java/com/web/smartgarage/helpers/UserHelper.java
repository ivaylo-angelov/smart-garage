package com.web.smartgarage.helpers;


import com.web.smartgarage.models.Role;
import com.web.smartgarage.models.User;
import com.web.smartgarage.models.UserCredentials;

import java.util.Set;

public class UserHelper {


    public static User createMockCustomer() {
        var mockUser = new User();
        mockUser.setId(1L);
        mockUser.setUserCredentials(new UserCredentials());
        mockUser.getUserCredentials().setId(1L);
        mockUser.getUserCredentials().setEmail("mock@email");
        mockUser.getUserCredentials().setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setPhone("0878852258");
        mockUser.setRoles(Set.of(createRoleClient()));
        return mockUser;
    }

    public static User createMockEmployee() {
        var mockUser = new User();
        mockUser.setId(1L);
        mockUser.setUserCredentials(new UserCredentials());
        mockUser.getUserCredentials().setId(2L);
        mockUser.getUserCredentials().setEmail("mock@email");
        mockUser.getUserCredentials().setPassword("MockPassword");
        mockUser.setFirstName("MockFirstName");
        mockUser.setLastName("MockLastName");
        mockUser.setPhone("0878852258");
        mockUser.setRoles(Set.of(createRoleAdministrator()));
        return mockUser;
    }

    private static Role createRoleClient() {
        Role role = new Role();
        role.setId(1L);
        role.setName("ROLE_CLIENT");
        return role;
    }

    private static Role createRoleAdministrator() {
        Role role = new Role();
        role.setId(1L);
        role.setName("ROLE_ADMINISTRATOR");
        return role;
    }
}
