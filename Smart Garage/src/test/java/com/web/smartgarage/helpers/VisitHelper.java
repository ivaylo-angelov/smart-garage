package com.web.smartgarage.helpers;


import com.web.smartgarage.models.Service;
import com.web.smartgarage.models.Visit;
import com.web.smartgarage.models.VisitStatus;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

public class VisitHelper {

    public static Visit createMockVisit() {
        Visit visit = new Visit();
        visit.setId(1L);
        visit.setStatus(VisitStatus.PREPARING);
        visit.setDate(LocalDate.now());
        visit.setVehicle(VehicleHelper.createMockVehicle());
        visit.setServices(createMockVisitService());
        visit.setTotalPrice(10.0);
        return visit;
    }

    public static Set<Service> createMockVisitService() {
        Set<Service> mockVisitServices = new HashSet<>();
        Service mockService = new Service();
        mockService.setPrice(5.5);
        mockService.setName("mockServiceName");
        mockService.setId(1L);

        Service mockService2 = new Service();
        mockService2.setPrice(10.5);
        mockService2.setName("mockServiceName2");
        mockService2.setId(2L);

        mockVisitServices.add(mockService);
        mockVisitServices.add(mockService2);

        return mockVisitServices;
    }
}
