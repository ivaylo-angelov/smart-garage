package com.web.smartgarage.services;


import com.web.smartgarage.exceptions.DuplicateEntityException;
import com.web.smartgarage.exceptions.EntityNotFoundException;
import com.web.smartgarage.helpers.VehicleHelper;
import com.web.smartgarage.models.Vehicle;
import com.web.smartgarage.models.Visit;
import com.web.smartgarage.models.searchparameters.VehicleSearchParameters;
import com.web.smartgarage.repositories.contracts.VehicleRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;
import static com.web.smartgarage.helpers.VisitHelper.createMockVisit;

@ExtendWith(MockitoExtension.class)
public class VehicleServiceImplTests {
    @Mock
    VehicleRepository repository;

    @InjectMocks
    VehicleServiceImpl service;

    @Test
    public void getById_Should_Call_Repository() {
        when(repository.getById(1L))
                .thenReturn(any(Vehicle.class));

        service.getById(1L);

        verify(repository, times(1)).getById(1L);
    }

    @Test
    public void getById_Should_Throw_WhenIdDoesNotExists() {
        when(repository.getById(100L))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getById(100L));
    }

    @Test
    public void getAll_Should_Call_Repository() {
        VehicleSearchParameters vsp = new VehicleSearchParameters();
        when(repository.getAll(vsp))
                .thenReturn(new ArrayList<>());

        service.getAll(vsp);

        verify(repository, times(1)).getAll(vsp);
    }

    @Test
    public void getByLicensePlate_Should_Call_Repository() {
        String mockLicensePlate = "mockLicensePlate";

        when(repository.getByLicensePlate(mockLicensePlate))
                .thenReturn(any(Vehicle.class));

        service.getByLicensePlate(mockLicensePlate);

        verify(repository, times(1)).getByLicensePlate(mockLicensePlate);
    }

    @Test
    public void getByLicensePlate_Should_Throw_WhenVehicleWithSameLicensePlateDoesNotExists() {
        String mockLicensePlate = "mockLicensePlate";
        when(repository.getByLicensePlate(mockLicensePlate))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getByLicensePlate(mockLicensePlate));
    }

    @Test
    public void create_Should_Throw_WhenVehicleWithSameLicensePlateExist() {
        var mockVehicle = VehicleHelper.createMockVehicle();

        when(repository
                .isVehicleExisting(mockVehicle.getLicensePlate(),
                        "licensePlate",
                        -1L))
                .thenReturn(true);

        assertThrows(DuplicateEntityException.class, () -> service.create(mockVehicle));
    }

    @Test
    public void create_Should_Throw_WhenVehicleWithSameVinExist() {
        var mockVehicle = VehicleHelper.createMockVehicle();

        when(repository
                .isVehicleExisting(mockVehicle.getLicensePlate(),
                        "licensePlate",
                        -1L))
                .thenReturn(false);

        when(repository
                .isVehicleExisting(mockVehicle.getVin(),
                        "vin",
                        -1L))
                .thenReturn(true);

        assertThrows(DuplicateEntityException.class, () -> service.create(mockVehicle));
    }

    @Test
    public void create_Should_Call_Repository_WhenVehicleDoesNotExist() {
        var mockVehicle = VehicleHelper.createMockVehicle();

        when(repository
                .isVehicleExisting(mockVehicle.getLicensePlate(),
                        "licensePlate",
                        -1L))
                .thenReturn(false);

        when(repository
                .isVehicleExisting(mockVehicle.getVin(),
                        "vin",
                        -1L))
                .thenReturn(false);

        service.create(mockVehicle);

        verify(repository, times(1)).create(mockVehicle);
    }


    @Test
    public void update_Should_Throw_WhenVehicleWithSameLicensePlateExist() {
        var mockVehicle = VehicleHelper.createMockVehicle();

        when(repository
                .isVehicleExisting(mockVehicle.getLicensePlate(),
                        "licensePlate",
                        mockVehicle.getId()))
                .thenReturn(true);


        assertThrows(DuplicateEntityException.class, () -> service.update(mockVehicle));

    }

    @Test
    public void update_Should_Throw_WhenVehicleWithSameVinExist() {
        var mockVehicle = VehicleHelper.createMockVehicle();

        when(repository
                .isVehicleExisting(mockVehicle.getLicensePlate(),
                        "licensePlate",
                        mockVehicle.getId()))
                .thenReturn(false);

        when(repository
                .isVehicleExisting(mockVehicle.getVin(),
                        "vin",
                        mockVehicle.getId()))
                .thenReturn(true);

        assertThrows(DuplicateEntityException.class, () -> service.update(mockVehicle));
    }

    @Test
    public void update_Should_Call_Repository_WhenVehicleDoesNotExist() {
        var mockVehicle = VehicleHelper.createMockVehicle();

        when(repository
                .isVehicleExisting(mockVehicle.getLicensePlate(),
                        "licensePlate",
                        mockVehicle.getId()))
                .thenReturn(false);

        when(repository
                .isVehicleExisting(mockVehicle.getVin(),
                        "vin",
                        mockVehicle.getId()))
                .thenReturn(false);

        service.update(mockVehicle);

        verify(repository, times(1)).update(mockVehicle);
    }

    @Test
    public void getByVin_Should_Throw_When_VehicleDoesNotExist() {
        var mockVehicle = VehicleHelper.createMockVehicle();

        when(service.getByVin(mockVehicle.getVin())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getByVin(mockVehicle.getVin()));
    }

    @Test
    public void findPaginated_Should_Return_Pages() {
        Page<Vehicle> page = service.findPaginated(PageRequest.of(1, 1), new VehicleSearchParameters());

        assertThat(page.getSize(), equalTo(1));
    }


    private List<Visit> mockVisits() {
        var mockVisit = createMockVisit();
        var mockVisit2 = createMockVisit();

        return List.of(mockVisit, mockVisit2);
    }
}
