package com.web.smartgarage.services;

import com.web.smartgarage.exceptions.EntityNotFoundException;
import com.web.smartgarage.models.Visit;
import com.web.smartgarage.models.VisitStatus;
import com.web.smartgarage.models.searchparameters.VisitSearchParameters;
import com.web.smartgarage.services.contracts.MailSender;
import com.web.smartgarage.services.contracts.ServiceService;
import com.web.smartgarage.services.contracts.SmsSender;
import com.web.smartgarage.repositories.contracts.VisitRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.mail.MessagingException;
import java.util.ArrayList;
import java.util.List;

import static com.web.smartgarage.helpers.VisitHelper.createMockVisit;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;


@ExtendWith(MockitoExtension.class)
public class VisitServiceImplTests {

    @Mock
    VisitRepository repository;

    @Mock
    ServiceService serviceService;

    @Mock
    SmsSender smsSender;

    @Mock
    MailSender mailSender;

    @InjectMocks
    VisitServiceImpl service;

    @InjectMocks
    SmsService smsService;
    @Test
    public void getById_Should_Call_Repository() {
        when(repository.getById(1L))
                .thenReturn(any(Visit.class));

        service.getById(1L);

        verify(repository, times(1)).getById(1L);
    }

    @Test
    public void getById_Should_Throw_When_VisitDoesNotExist() {
        when(repository.getById(anyLong())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> service.getById(anyLong()));
    }

    @Test
    public void getAll_Should_Returns_ListOfVisits() {
        var mockVsp = new VisitSearchParameters();
        var mockVisit = createMockVisit();
        var mockVisits = new ArrayList<Visit>();
        mockVisits.add(mockVisit);

        when(repository.getAll(mockVsp)).thenReturn(mockVisits);

        assertEquals(service.getAll(mockVsp).size(), 1);
    }

    @Test
    public void getByDate_Should_Call_Repository_When_VisitExists() {
        var mockDate = createMockVisit().getDate();

        when(repository.getByDate(mockDate)).thenReturn(any(Visit.class));

        service.getByDate(mockDate);

        verify(repository, times(1)).getByDate(mockDate);
    }

    @Test
    public void getByDate_Should_Throw_When_VisitDoesNotExist() {
        var mockDate = createMockVisit().getDate();

        when(repository.getByDate(mockDate)).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> service.getByDate(mockDate));
    }

    @Test
    public void create_Should_Call_Repository_When_VisitIsValid() {
        var mockVisit = createMockVisit();

        when(repository.create(mockVisit)).thenReturn(any(Visit.class));

        service.create(mockVisit);

        verify(repository, times(1)).create(mockVisit);
    }


    @Test
    public void update_Should_Call_Repository_When_VisitIsValid() throws MessagingException {
        var mockVisit = createMockVisit();

        when(repository.update(mockVisit)).thenReturn(mockVisit);

        service.update(mockVisit);

        verify(repository, times(1)).update(mockVisit);
    }

    @Test
    public void update_Should_Call_Repository_And_SendEmail_When_VisitIsValid() throws MessagingException {
        var mockVisit = createMockVisit();
        mockVisit.setStatus(VisitStatus.FINISHED);

        doNothing().when(mailSender).sendEmailWithAttachment(isA(String.class), isA(String.class),
                isA(String.class), isA(String.class));

        when(repository.update(mockVisit)).thenReturn(mockVisit);

        service.update(mockVisit);

        verify(repository, times(1)).update(mockVisit);
    }

    @Test
    public void delete_Should_Call_Repository_When_Visit_Exists() {

        service.delete(anyLong());

        verify(repository, times(1)).delete(anyLong());
    }

    @Test
    public void findPaginated_Should_Return_Pages() {
        Page<Visit> page = service.findPaginated(PageRequest.of(1, 1), new VisitSearchParameters());
        assertThat(page.getSize(), equalTo(1));
    }

    private List<Visit> mockVisits() {
        var mockVisit = createMockVisit();
        var anotherMockVisit = createMockVisit();

        return List.of(mockVisit, anotherMockVisit);
    }

}
