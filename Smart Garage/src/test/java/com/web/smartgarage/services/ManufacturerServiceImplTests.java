package com.web.smartgarage.services;

import com.web.smartgarage.exceptions.DuplicateEntityException;
import com.web.smartgarage.exceptions.EntityNotFoundException;
import com.web.smartgarage.helpers.VehicleHelper;
import com.web.smartgarage.models.Manufacturer;
import com.web.smartgarage.models.searchparameters.ManufacturerSearchParameters;
import com.web.smartgarage.repositories.contracts.ManufacturerRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ManufacturerServiceImplTests {

    @Mock
    ManufacturerRepository repository;

    @InjectMocks
    ManufacturerServiceImpl service;


    @Test
    public void getAll_Should_Call_Repository() {
        ManufacturerSearchParameters msp = new ManufacturerSearchParameters();

        when(repository.getAll(msp)).thenReturn(new ArrayList<>());

        service.getAll(msp);

        verify(repository, times(1)).getAll(msp);
    }

    @Test
    public void getById_Should_Call_Repository_When_Manufacturer() {
        var mockManufacturer = VehicleHelper.createMockManufacturer();

        when(repository.getById(mockManufacturer.getId()))
                .thenReturn(mockManufacturer);

        service.getById(mockManufacturer.getId());

        verify(repository, times(1)).getById(1L);
    }

    @Test
    public void getById_Should_Throw_When_ManufacturerDoesNotExist() {
        when(repository.getById(100L))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getById(100L));
    }

    @Test
    public void getByName_Should_Call_Repository_When_ManufacturerExist() {
        var mockManufacturer = VehicleHelper.createMockManufacturer();

        when(repository.getByName(mockManufacturer.getName())).thenReturn(mockManufacturer);

        service.getByName(mockManufacturer.getName());

        verify(repository, times(1)).getByName(mockManufacturer.getName());
    }

    @Test
    public void getByName_Should_Throw_When_ManufacturerDoesNotExist() {
        var mockManufacturer = VehicleHelper.createMockManufacturer();

        when(repository.getByName(mockManufacturer.getName())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class, () -> service.getByName(mockManufacturer.getName()));
    }

    @Test
    public void create_Should_Call_Repository_When_NameIsValid() {
        var mockManufacturer = VehicleHelper.createMockManufacturer();

        when(repository.getByName(mockManufacturer.getName())).thenThrow(EntityNotFoundException.class);

        when(repository.create(mockManufacturer)).thenReturn(mockManufacturer);

        service.create(mockManufacturer);

        verify(repository, times(1)).create(mockManufacturer);
    }

    @Test
    public void create_Should_Throw_When_ManufacturerWithSameExist() {
        var mockManufacturer = VehicleHelper.createMockManufacturer();

        when(repository.getByName(mockManufacturer.getName())).thenReturn(mockManufacturer);

        assertThrows(DuplicateEntityException.class,
                () -> service.create(mockManufacturer));
    }

    @Test
    public void update_Should_Call_Repository_When_ManufacturerNameIsValid() {
        var mockManufacturer = VehicleHelper.createMockManufacturer();

        when(repository.getByName(mockManufacturer.getName())).thenReturn(mockManufacturer);

        when(repository.update(mockManufacturer)).thenReturn(mockManufacturer);

        service.update(mockManufacturer);

        verify(repository, times(1)).update(mockManufacturer);
    }

    @Test
    public void update_Should_Call_Repository_When_ManufacturerIsValid() {
        var mockManufacturer = VehicleHelper.createMockManufacturer();

        when(repository.getByName(mockManufacturer.getName())).thenThrow(EntityNotFoundException.class);

        when(repository.update(mockManufacturer)).thenReturn(mockManufacturer);

        service.update(mockManufacturer);

        verify(repository, times(1)).update(mockManufacturer);
    }

    @Test
    public void update_Should_Throw_When_ManufacturerWithSameNameExist() {
        var mockManufacturer = VehicleHelper.createMockManufacturer();
        var mockManufacturer2 = VehicleHelper.createMockManufacturer();
        mockManufacturer2.setId(17L);

        when(repository.getByName(mockManufacturer.getName())).thenReturn(mockManufacturer2);

        assertThrows(DuplicateEntityException.class,
                () -> service.update(mockManufacturer));
    }

    @Test
    public void findPaginated_Should_Return_Pages() {
        Page<Manufacturer> page = service.findPaginated(PageRequest.of(1, 1));
        assertThat(page.getSize(), equalTo(1));
    }

    @Test
    public void delete_Should_Call_Repository() {
        var mockManufacturer = VehicleHelper.createMockManufacturer();

        service.delete(mockManufacturer.getId());

        verify(repository, times(1)).delete(mockManufacturer.getId());
    }
}
