package com.web.smartgarage.services;

import com.web.smartgarage.exceptions.DuplicateEntityException;
import com.web.smartgarage.exceptions.EntityNotFoundException;
import com.web.smartgarage.helpers.VehicleHelper;
import com.web.smartgarage.models.Model;
import com.web.smartgarage.models.searchparameters.ModelSearchParameters;
import com.web.smartgarage.repositories.contracts.ModelRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.ArrayList;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class ModelServiceImplTests {
    @Mock
    ModelRepository repository;

    @InjectMocks
    ModelServiceImpl service;

    @Test
    public void getAll_Should_Call_Repository() {
        ModelSearchParameters msp = new ModelSearchParameters();

        when(repository.getAll(msp)).thenReturn(new ArrayList<>());

        service.getAll(msp);

        verify(repository, times(1)).getAll(msp);
    }

    @Test
    public void getById_Should_Call_Repository_When_ModelExists() {
        var mockModel = VehicleHelper.createMockModel();

        when(repository.getById(mockModel.getId())).thenReturn(mockModel);

        service.getById(mockModel.getId());

        verify(repository, times(1)).getById(mockModel.getId());
    }

    @Test
    public void getById_Should_Throw_When_ModelDoesNotExist() {
        when(repository.getById(100L))
                .thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getById(100L));
    }

    @Test
    public void getByName_Should_Call_Repository_When_ModelExists() {
        var mockModel = VehicleHelper.createMockModel();

        when(repository.getByName(mockModel.getName())).thenReturn(mockModel);

        service.getByName(mockModel.getName());

        verify(repository, times(1)).getByName(mockModel.getName());
    }

    @Test
    public void getByName_Should_Throw_When_ModelDoesNotExist() {
        var mockModel = VehicleHelper.createMockModel();

        when(repository.getByName(mockModel.getName())).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getByName(mockModel.getName()));
    }

    @Test
    public void create_Should_Throw_When_ModelWithSameExists() {
        var mockModel = VehicleHelper.createMockModel();

        when(repository.getByName(mockModel.getName())).thenReturn(mockModel);

        assertThrows(DuplicateEntityException.class,
                () -> service.create(mockModel));
    }

    @Test
    public void create_Should_Call_Repository_When_ModelIsValid() {
        var mockModel = VehicleHelper.createMockModel();

        when(repository.getByName(mockModel.getName())).thenThrow(EntityNotFoundException.class);

        when(repository.create(mockModel)).thenReturn(mockModel);

        service.create(mockModel);

        verify(repository, times(1)).create(mockModel);
    }

    @Test
    public void update_Should_Throw_When_ModelWithSameExists() {
        var mockModel = VehicleHelper.createMockModel();
        var mockModel2 = VehicleHelper.createMockModel();
        mockModel2.setId(102L);

        when(repository.getByName(mockModel.getName())).thenReturn(mockModel2);

        assertThrows(DuplicateEntityException.class,
                () -> service.update(mockModel));
    }

    @Test
    public void update_Should_Call_Repository_When_ModelIsValid() {
        var mockModel = VehicleHelper.createMockModel();

        when(repository.getByName(mockModel.getName())).thenReturn(mockModel);

        when(repository.update(mockModel)).thenReturn(mockModel);

        service.update(mockModel);

        verify(repository, times(1)).update(mockModel);
    }

    @Test
    public void update_Should_Call_Repository_When_ModelNameIsValid() {
        var mockModel = VehicleHelper.createMockModel();

        when(repository.getByName(mockModel.getName())).thenThrow(EntityNotFoundException.class);

        when(repository.update(mockModel)).thenReturn(mockModel);

        service.update(mockModel);

        verify(repository, times(1)).update(mockModel);
    }

    @Test
    public void delete_Should_Call_Repository_When_ManufacturerIsValid() {
        var mockManufacturer = VehicleHelper.createMockManufacturer();

        service.delete(1L);

        verify(repository, times(1)).delete(mockManufacturer.getId());
    }

    @Test
    public void findPaginated_Should_Return_Pages() {
        Page<Model> page = service.findPaginated(PageRequest.of(1, 1), new ModelSearchParameters());
        assertThat(page.getSize(), equalTo(1));
    }
}
