package com.web.smartgarage.services;

import com.web.smartgarage.exceptions.DuplicateEntityException;
import com.web.smartgarage.exceptions.EntityNotFoundException;
import com.web.smartgarage.helpers.UserHelper;
import com.web.smartgarage.helpers.VehicleHelper;
import com.web.smartgarage.models.User;
import com.web.smartgarage.models.Vehicle;
import com.web.smartgarage.models.searchparameters.UserSearchParameters;
import com.web.smartgarage.services.contracts.MailSender;
import com.web.smartgarage.services.contracts.VehicleService;
import com.web.smartgarage.repositories.contracts.UserRepository;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceImplTests {

    @Mock
    UserRepository repository;

    @Mock
    MailSender mailSender;

    @Mock
    VehicleService vehicleService;

    @InjectMocks
    UserServiceImpl service;

    @Test
    public void getAll_Should_Call_Repository() {

        UserSearchParameters usp = new UserSearchParameters();

        service.getAll(usp);

        verify(repository, times(1)).getAll(usp);
    }

    @Test
    public void getById_Should_Call_Repository_WhenCustomerExists() {
        when(repository.getById(1L)).thenReturn(any(User.class));

        service.getById(1L);

        verify(repository, times(1)).getById(1L);
    }

    @Test
    public void getById_Should_Throw_When_CustomerDoesNotExist() {
        when(repository.getById(100L)).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getById(100L));
    }

    @Test
    public void getByPhone_Should_Call_Repository_WhenCustomerExists() {
        when(repository.getByPhone("+359876127140")).thenReturn(any(User.class));

        service.getByPhone("+359876127140");

        verify(repository, times(1)).getByPhone("+359876127140");
    }

    @Test
    public void getByPhone_Should_Throw_When_CustomerWithPhoneDoesNotExist() {
        when(repository.getByPhone("+359876127140")).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getByPhone("+359876127140"));
    }

    @Test
    public void getByEmail_Should_Call_Repository_WhenCustomerExists() {
        when(repository.getByEmail("mock@email.com")).thenReturn(any(User.class));

        service.getByEmail("mock@email.com");

        verify(repository, times(1)).getByEmail("mock@email.com");
    }

    @Test
    public void getByEmail_Should_Throw_When_CustomerWithEmailDoesNotExist() {
        when(repository.getByEmail("mock@email.com")).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.getByEmail("mock@email.com"));
    }

    @Test
    public void create_Client_Should_Throw_When_EmailIsDuplicated() throws UnsupportedEncodingException, MessagingException {
        var mockCustomer = UserHelper.createMockCustomer();

        when(repository.getByEmail(mockCustomer.getUserCredentials().getEmail())).thenReturn(mockCustomer);

        assertThrows(DuplicateEntityException.class,
                () -> service.create(mockCustomer));
    }

    @Test
    public void create_Employee_Should_Throw_When_EmailIsDuplicated() throws UnsupportedEncodingException, MessagingException {
        var mockEmployee = UserHelper.createMockEmployee();

        when(repository.getByEmail(mockEmployee.getUserCredentials().getEmail())).thenReturn(mockEmployee);

        assertThrows(DuplicateEntityException.class,
                () -> service.create(mockEmployee));
    }

    @Test
    public void createClient_Should_Throw_When_PhoneIsDuplicated() {
        var mockCustomer = UserHelper.createMockCustomer();

        when(repository.getByEmail(mockCustomer.getUserCredentials().getEmail())).thenThrow(EntityNotFoundException.class);
        when(repository.getByPhone(mockCustomer.getPhone())).thenReturn(mockCustomer);

        assertThrows(DuplicateEntityException.class,
                () -> service.create(mockCustomer));
    }

    @Test
    public void createEmployee_Should_Throw_When_PhoneIsDuplicated() {
        var mockEmployee = UserHelper.createMockEmployee();

        when(repository.getByEmail(mockEmployee.getUserCredentials().getEmail())).thenThrow(EntityNotFoundException.class);
        when(repository.getByPhone(mockEmployee.getPhone())).thenReturn(mockEmployee);

        assertThrows(DuplicateEntityException.class,
                () -> service.create(mockEmployee));
    }

    @Test
    public void createClient_Should_Call_Repository_WhenClientIsValid() throws UnsupportedEncodingException, MessagingException {
        var mockCustomer = UserHelper.createMockCustomer();

        when(repository.getByEmail(mockCustomer.getUserCredentials().getEmail())).thenThrow(EntityNotFoundException.class);
        when(repository.getByPhone(mockCustomer.getPhone())).thenThrow(EntityNotFoundException.class);

        doNothing().when(mailSender).sendEmail(isA(String.class), isA(String.class));

        service.create(mockCustomer);

        verify(repository, times(1)).create(mockCustomer);
    }

    @Test
    public void createEmployee_Should_Call_Repository_WhenEmployeeIsValid() throws UnsupportedEncodingException, MessagingException {
        var mockEmployee = UserHelper.createMockEmployee();

        when(repository.getByEmail(mockEmployee.getUserCredentials().getEmail())).thenThrow(EntityNotFoundException.class);
        when(repository.getByPhone(mockEmployee.getPhone())).thenThrow(EntityNotFoundException.class);

        service.create(mockEmployee);

        verify(repository, times(1)).create(mockEmployee);
    }

    @Test
    public void updateClient_Should_Throw_When_PhoneIsDuplicated() {
        var mockCustomer = UserHelper.createMockCustomer();
        var anotherMockCustomer = UserHelper.createMockCustomer();
        anotherMockCustomer.setId(2L);

        when(repository.getByEmail(mockCustomer.getUserCredentials().getEmail())).thenThrow(EntityNotFoundException.class);
        when(repository.getByPhone(mockCustomer.getPhone())).thenReturn(anotherMockCustomer);

        assertThrows(DuplicateEntityException.class,
                () -> service.update(mockCustomer));
    }

    @Test
    public void updateEmployee_Should_Throw_When_PhoneIsDuplicated() {
        var mockEmployee = UserHelper.createMockEmployee();
        var anotherMockEmployee = UserHelper.createMockEmployee();
        anotherMockEmployee.setId(2L);

        when(repository.getByEmail(mockEmployee.getUserCredentials().getEmail())).thenThrow(EntityNotFoundException.class);
        when(repository.getByPhone(mockEmployee.getPhone())).thenReturn(anotherMockEmployee);

        assertThrows(DuplicateEntityException.class,
                () -> service.update(mockEmployee));
    }

    @Test
    public void updateClient_Should_Throw_When_EmailIsDuplicated() {
        var mockCustomer = UserHelper.createMockCustomer();
        var anotherMockCustomer = UserHelper.createMockCustomer();
        anotherMockCustomer.setId(2L);

        when(repository.getByEmail(mockCustomer.getUserCredentials().getEmail())).thenReturn(anotherMockCustomer);

        assertThrows(DuplicateEntityException.class,
                () -> service.update(mockCustomer));
    }

    @Test
    public void updateEmployee_Should_Throw_When_EmailIsDuplicated() {
        var mockEmployee = UserHelper.createMockEmployee();
        var anotherMockEmployee = UserHelper.createMockEmployee();
        anotherMockEmployee.setId(2L);

        when(repository.getByEmail(mockEmployee.getUserCredentials().getEmail())).thenReturn(anotherMockEmployee);

        assertThrows(DuplicateEntityException.class,
                () -> service.update(mockEmployee));
    }

    @Test
    public void update_Should_Call_Repository_WhenCustomerIsValid() {
        var mockCustomer = UserHelper.createMockCustomer();


        when(repository.getByEmail(mockCustomer.getUserCredentials().getEmail())).thenReturn(mockCustomer);
        when(repository.getByPhone(mockCustomer.getPhone())).thenReturn(mockCustomer);

        service.update(mockCustomer);

        verify(repository, times(1)).update(mockCustomer);
    }

    @Test
    public void update_Should_Call_Repository_WhenEmployeeIsValid() {
        var mockEmployee = UserHelper.createMockEmployee();


        when(repository.getByEmail(mockEmployee.getUserCredentials().getEmail())).thenReturn(mockEmployee);
        when(repository.getByPhone(mockEmployee.getPhone())).thenReturn(mockEmployee);

        service.update(mockEmployee);

        verify(repository, times(1)).update(mockEmployee);
    }


    @Test
    public void delete_Should_Throw_When_UserDoesNotExist() {
        when(repository.getById(1L)).thenThrow(EntityNotFoundException.class);

        assertThrows(EntityNotFoundException.class,
                () -> service.delete(1L));
    }


    @Test
    public void delete_Should_Call_Repository() {
        var mockCustomer = UserHelper.createMockCustomer();

        when(repository.getById(mockCustomer.getId())).thenReturn(mockCustomer);

        service.delete(mockCustomer.getId());

        verify(repository, times(1)).delete(mockCustomer.getId());
    }

    @Test
    public void findPaginated_Should_Return_Pages() {
        Page<User> page = service.findPaginated(PageRequest.of(1, 1),
                new UserSearchParameters());

        assertThat(page.getSize(), equalTo(1));
    }

    private List<Vehicle> mockClientVehicles() {
        var mockVehicle = VehicleHelper.createMockVehicle();
        var mockVehicle2 = VehicleHelper.createMockVehicle();
        return List.of(mockVehicle, mockVehicle2);
    }

}