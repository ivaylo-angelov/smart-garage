package com.web.smartgarage.models;

public enum VehicleType {
    CAR("Car"),
    TRUCK("Truck"),
    BUS("Bus");

    private String value;

    VehicleType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
