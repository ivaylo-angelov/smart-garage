package com.web.smartgarage.models.dto;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

public class StatusDto {

    @NotNull(message = "Status's name cannot be null.")
    @Size(min = 5, max = 20, message = "Status's name should be between 5 and 20 symbols.")
    private String name;

    public StatusDto() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
