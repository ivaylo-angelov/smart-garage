package com.web.smartgarage.models;


public enum VisitStatus {

    PREPARING("Preparing"),
    FINISHED("Finished"),
    IN_PROCESS("In Process"),
    READY_FOR_PICK_UP("Ready for pick up");

    private final String value;

    VisitStatus(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }


}
