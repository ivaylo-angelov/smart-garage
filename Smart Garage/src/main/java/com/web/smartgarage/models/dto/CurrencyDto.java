package com.web.smartgarage.models.dto;

public class CurrencyDto {

    private String name;

    public CurrencyDto(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
