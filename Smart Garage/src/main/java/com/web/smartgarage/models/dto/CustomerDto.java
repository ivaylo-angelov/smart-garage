package com.web.smartgarage.models.dto;

import com.web.smartgarage.models.UserDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class CustomerDto {

    @Valid
    @NotNull(message = "User should not be null.")
    private UserDto user;

    public CustomerDto() {
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }
}
