package com.web.smartgarage.models.dto;

import com.web.smartgarage.models.UserDto;

import javax.validation.Valid;
import javax.validation.constraints.NotNull;

public class EmployeeDto {

    @Valid
    @NotNull(message = "User should not be null.")
    private UserDto user;

    public EmployeeDto() {

    }

    public EmployeeDto(UserDto user) {
        setUser(user);
    }

    public UserDto getUser() {
        return user;
    }

    public void setUser(UserDto user) {
        this.user = user;
    }
}
