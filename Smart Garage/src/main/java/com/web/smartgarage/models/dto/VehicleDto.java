package com.web.smartgarage.models.dto;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;

import javax.validation.Valid;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;
import javax.validation.constraints.Size;

@AllArgsConstructor
@NoArgsConstructor
public class VehicleDto {

    @NotNull(message = "License plate cannot be null.")
    @Size(min = 6, max = 8, message = "License plate should be exactly 10 symbols.")
    private String licensePlate;

    @NotNull(message = "Year cannot be null.")
    @Positive(message = "Year should be positive.")
    private Long year;

    @NotNull(message = "Vin cannot be null.")
    @Size(min = 17, max = 17, message = "Vin should be exactly 17 symbols.")
    private String vin;

    @NotNull(message = "Model id cannot be null.")
    @Positive(message = "Model id should be positive.")
    private Long modelId;

    @NotNull(message = "Vehicle type cannot be null.")
    private String type;

    @Email
    @Valid
    @NotNull(message = "Customer email cannot be null.")
    private String email;

    public String getLicensePlate() {
        return licensePlate;
    }

    public void setLicensePlate(String licensePlate) {
        this.licensePlate = licensePlate;
    }

    public Long getYear() {
        return year;
    }

    public void setYear(Long year) {
        this.year = year;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public Long getModelId() {
        return modelId;
    }

    public void setModelId(Long modelId) {
        this.modelId = modelId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
