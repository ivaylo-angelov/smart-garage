package com.web.smartgarage.services.contracts;

import com.web.smartgarage.models.searchparameters.ModelSearchParameters;
import com.web.smartgarage.models.Model;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ModelService {
    List<Model> getAll(ModelSearchParameters modelSearchParameters);

    Model getById(Long id);

    Model getByName(String name);

    Model create(Model model);

    Model update(Model model);

    void delete(Long id);

    Page<Model> findPaginated(Pageable pageable, ModelSearchParameters modelSearchParameters);
}
