package com.web.smartgarage.services.contracts;

import java.util.Set;

public interface CurrencyConverterService {

    Set<String> getAllCurrencies();

    Double convert(String from, String to, Double amount);
}
