package com.web.smartgarage.services.contracts;

import com.web.smartgarage.models.searchparameters.ServiceSearchParameters;
import com.web.smartgarage.models.Service;
import com.web.smartgarage.models.Visit;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ServiceService {

    List<Service> getAll(ServiceSearchParameters ssp);

    Page<Service> findPaginated(Pageable pageable, ServiceSearchParameters serviceSearchParameters);

    Service getById(Long id);

    List<Service> getServicesByVisit(Visit visit);

    Service getByName(String name);

    Service create(Service service);

    Service update(Service service);

    void delete(Long id);
}
