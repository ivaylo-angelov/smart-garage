package com.web.smartgarage.services;

import com.web.smartgarage.services.contracts.RoleService;
import com.web.smartgarage.models.Role;
import com.web.smartgarage.repositories.contracts.RoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RoleServiceImpl implements RoleService {

    private final RoleRepository repository;

    @Autowired
    public RoleServiceImpl(RoleRepository repository) {
        this.repository = repository;
    }

    @Override
    public Role getByName(String name) {
        return repository.getByName(name);
    }
}
