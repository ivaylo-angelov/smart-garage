package com.web.smartgarage.services;

import com.web.smartgarage.config.SecurityConfig;
import com.web.smartgarage.models.User;
import com.web.smartgarage.repositories.contracts.UserRepository;
import com.web.smartgarage.services.contracts.ResetPasswordService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class RestPasswordServiceImpl implements ResetPasswordService {

    private final UserRepository userRepository;

    @Autowired
    public RestPasswordServiceImpl(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    @Override
    public void updateResetPasswordToken(String token, String email) {
        User user = userRepository.getByEmail(email);
        user.getUserCredentials().setResetPassword(token);
        userRepository.update(user);
    }

    @Override
    public User getUserByResetPasswordToken(String resetPasswordToken) {
        return userRepository.findByResetPassword(resetPasswordToken);
    }

    @Override
    public void updatePassword(User user, String newPassword) {
        String encodePassword = SecurityConfig.passwordEncoder().encode(newPassword);

        user.getUserCredentials().setPassword(encodePassword);
        user.getUserCredentials().setResetPassword(null);

        userRepository.update(user);
    }
}
