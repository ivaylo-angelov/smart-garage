package com.web.smartgarage.services.contracts;

import com.web.smartgarage.models.Visit;
import com.web.smartgarage.models.searchparameters.VisitSearchParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import javax.mail.MessagingException;
import java.time.LocalDate;
import java.util.List;

public interface VisitService {

    List<Visit> getAll(VisitSearchParameters vsp);

    Visit getById(Long id);

    Visit getByDate(LocalDate date);

    Visit create(Visit visit);

    Visit update(Visit visit) throws MessagingException;

    Page<Visit> findPaginated(Pageable pageable, VisitSearchParameters vsp);

    void delete(Long id);
}
