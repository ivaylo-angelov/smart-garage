package com.web.smartgarage.services;

import com.web.smartgarage.models.searchparameters.UserSearchParameters;
import com.web.smartgarage.models.searchparameters.VehicleSearchParameters;
import com.web.smartgarage.services.contracts.MailSender;
import com.web.smartgarage.services.contracts.UserService;
import com.web.smartgarage.services.contracts.VehicleService;
import com.web.smartgarage.config.SecurityConfig;
import com.web.smartgarage.exceptions.DeletingEntityException;
import com.web.smartgarage.exceptions.DuplicateEntityException;
import com.web.smartgarage.exceptions.EntityNotFoundException;
import com.web.smartgarage.models.Role;
import com.web.smartgarage.models.User;
import com.web.smartgarage.models.Vehicle;
import com.web.smartgarage.repositories.contracts.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

@Service
public class UserServiceImpl implements UserService {

    private final MailSender mailSender;
    private final UserRepository userRepository;
    private final VehicleService vehicleService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, MailSender mailSender, VehicleService vehicleService) {
        this.userRepository = userRepository;
        this.mailSender = mailSender;
        this.vehicleService = vehicleService;
    }

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user;
        try {
            user = userRepository.getByEmail(email);
        } catch (EntityNotFoundException entityNotFoundException) {
            throw new UsernameNotFoundException("Invalid username or password.");
        }

        return new org.springframework.security.core.userdetails.User(user.getUserCredentials().getEmail(),
                user.getUserCredentials().getPassword(),
                mapRolesToAuthorities(user.getRoles()));
    }

    @Override
    public List<User> getAll(UserSearchParameters userSearchParameters) {
        return userRepository.getAll(userSearchParameters);
    }

    @Override
    public Page<User> findPaginated(Pageable pageable, UserSearchParameters userSearchParameters) {

        List<User> allUsers = getAll(userSearchParameters);

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<User> list;

        if (allUsers.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allUsers.size());
            list = allUsers.subList(startItem, toIndex);
        }

        return new PageImpl<User>(list, PageRequest.of(currentPage, pageSize), allUsers.size());
    }

    @Override
    public User getById(Long id) {
        return userRepository.getById(id);
    }

    @Override
    public User getByEmail(String email) {
        return userRepository.getByEmail(email);
    }

    @Override
    public User getByPhone(String phone) {
        return userRepository.getByPhone(phone);
    }

    @Override
    public User create(User user) throws UnsupportedEncodingException, MessagingException {

        User userToCreate;

        if (user.getRoles().stream().anyMatch(r -> r.getName().equals("ROLE_CLIENT"))) {
            userToCreate = createClient(user);
        } else {
            userToCreate = createEmployee(user);
        }

        return userToCreate;
    }

    @Override
    public User update(User user) {

        if (user.getRoles().stream().anyMatch(r -> r.getName().equals("ROLE_CLIENT"))) {
            updateClient(user);
        } else {
            updateEmployee(user);
        }

        return user;
    }

    @Override
    public void delete(Long id) {
        User userToDelete = getById(id);
        if (vehicleCount(userToDelete) != 0) {
            throw new DeletingEntityException("First You have to delete linked vehicle/s to this customer.");
        }

        userRepository.delete(id);
    }

    private Collection<? extends GrantedAuthority> mapRolesToAuthorities(Collection<Role> roles) {
        return roles.stream()
                .map(role -> new SimpleGrantedAuthority(role.getName()))
                .collect(Collectors.toList());
    }

    private User createClient(User user) throws UnsupportedEncodingException, MessagingException {
        validateEmail(user, -1L);
        validatePhone(user, -1L);
        String password = generateString();
        user.getUserCredentials().setPassword(password);
        encodePassword(user);
        mailSender.sendEmail(user.getUserCredentials().getEmail(), password);
        return userRepository.create(user);
    }

    private User createEmployee(User user) {
        validateEmail(user, -1L);
        validatePhone(user, -1L);
        encodePassword(user);
        return userRepository.create(user);
    }

    private void updateClient(User user) {
        validateEmail(user, user.getId());
        validatePhone(user, user.getId());

        if (user.getUserCredentials().getPassword() != null
                && !user.getUserCredentials().getPassword().equals("")
                && user.getUserCredentials().getPassword().length() <= 20) {
            encodePassword(user);
        }

        userRepository.update(user);
    }

    private void updateEmployee(User user) {
        validateEmail(user, user.getId());
        validatePhone(user, user.getId());

        if (user.getUserCredentials().getPassword() != null
                && !user.getUserCredentials().getPassword().equals("")
                && user.getUserCredentials().getPassword().length() <= 20) {
            encodePassword(user);
        }
        userRepository.update(user);
    }

    private String generateString() {
        int leftLimit = 48;
        int rightLimit = 122;
        int targetStringLength = 10;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .filter(i -> (i <= 57 || i >= 65) && (i <= 90 || i >= 97))
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    private void encodePassword(User user) {
        user.getUserCredentials().setPassword(
                SecurityConfig.passwordEncoder().encode(user.getUserCredentials().getPassword()));
    }

    private void validateEmail(User user, Long id) {
        try {
            User proxy = userRepository.getByEmail(user.getUserCredentials().getEmail());
            if (id.equals(-1L) && proxy != null) {
                throw new DuplicateEntityException("User", "email", user.getUserCredentials().getEmail());
            } else if (!proxy.getId().equals(id)) {
                throw new DuplicateEntityException("User", "email", user.getUserCredentials().getEmail());
            }
        } catch (EntityNotFoundException ignored) {
        }
    }

    private void validatePhone(User user, Long id) {
        try {
            User proxy = userRepository.getByPhone(user.getPhone());
            if (id.equals(-1L) && proxy != null) {
                throw new DuplicateEntityException("User", "phone", user.getPhone());
            } else if (!proxy.getId().equals(id)) {
                throw new DuplicateEntityException("User", "phone", user.getPhone());
            }
        } catch (EntityNotFoundException ignored) {
        }
    }

    private Integer vehicleCount(User user) {
        VehicleSearchParameters vsp = new VehicleSearchParameters();
        vsp.setCustomerEmail(user.getUserCredentials().getEmail());
        List<Vehicle> customerVehicles = vehicleService.getAll(vsp);
        return customerVehicles.size();
    }
}
