package com.web.smartgarage.services;

import com.web.smartgarage.models.searchparameters.VisitSearchParameters;
import com.web.smartgarage.services.contracts.MailSender;
import com.web.smartgarage.services.contracts.ServiceService;
import com.web.smartgarage.services.contracts.VisitService;
import com.web.smartgarage.exceptions.DuplicateEntityException;
import com.web.smartgarage.models.SmsRequest;
import com.web.smartgarage.models.Visit;
import com.web.smartgarage.repositories.contracts.VisitRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;
import java.time.LocalDate;
import java.util.Collections;
import java.util.List;
import java.util.Set;

@Service
public class VisitServiceImpl implements VisitService {

    private final VisitRepository repository;
    private final ServiceService serviceService;
    private final SmsService smsService;
    private final MailSender mailSender;

    public VisitServiceImpl(VisitRepository repository,
                            ServiceService serviceService,
                            SmsService smsService,
                            MailSender mailSender) {
        this.repository = repository;
        this.serviceService = serviceService;
        this.smsService = smsService;
        this.mailSender = mailSender;
    }

    @Override
    public List<Visit> getAll(VisitSearchParameters vsp) {
        return repository.getAll(vsp);
    }

    @Override
    public Visit getById(Long id) {
        return repository.getById(id);
    }

    @Override
    public Visit getByDate(LocalDate date) {
        return repository.getByDate(date);
    }

    @Override
    public Visit create(Visit visit) {
        isVehicleExists(visit);
        visit.setTotalPrice(calcTotalPrice(visit.getServices()));

        return repository.create(visit);
    }

    @Override
    public Visit update(Visit visit) throws MessagingException {
        isVehicleExists(visit);
        Double totalPrice = calcTotalPrice(visit.getServices());
        visit.setTotalPrice(totalPrice + visit.getTotalPrice());
        if (visit.getStatus().getValue().equals("Ready for pick up")) {

        }
        sendSmsNotification(visit);
        return repository.update(visit);
    }

    @Override
    public void delete(Long id) {
        repository.delete(id);
    }

    @Override
    public Page<Visit> findPaginated(Pageable pageable, VisitSearchParameters vsp) {

        List<Visit> allVisits = getAll(vsp);

        int pageSize = pageable.getPageSize();
        int currentPage = pageable.getPageNumber();
        int startItem = currentPage * pageSize;
        List<Visit> list;

        if (allVisits.size() < startItem) {
            list = Collections.emptyList();
        } else {
            int toIndex = Math.min(startItem + pageSize, allVisits.size());
            list = allVisits.subList(startItem, toIndex);
        }

        return new PageImpl<>(list, PageRequest.of(currentPage, pageSize), allVisits.size());
    }

    private void sendSmsNotification(Visit visit) throws MessagingException {
        smsService.sendSms(new SmsRequest(visit.getVehicle().getUser().getPhone(),
                "Vui lek manqk brat ela da si vzemesh kolata! <3"));
    }

    private void isVehicleExists(Visit visit) {
        VisitSearchParameters vsp = new VisitSearchParameters();
        vsp.setLicensePlate(visit.getVehicle().getLicensePlate());
        List<Visit> visits = getAll(vsp);

        if (visits.size() != 0) {
            for (Visit v : visits) {
                if (!v.getStatus().getValue().equals("Finished") && !v.getId().equals(visit.getId())) {
                    throw new DuplicateEntityException(String.format(
                            "Our mechanics are already working on vehicle with license plate %s",
                            visit.getVehicle().getLicensePlate()));
                }
            }
        }
    }

    private Double calcTotalPrice(Set<com.web.smartgarage.models.Service> services) {
        double totalPrice = 0;
        for (com.web.smartgarage.models.Service service : services) {
            totalPrice += service.getPrice();
        }

        return totalPrice;
    }
}
