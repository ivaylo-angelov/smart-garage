package com.web.smartgarage.services.contracts;

import com.web.smartgarage.models.searchparameters.ManufacturerSearchParameters;
import com.web.smartgarage.models.Manufacturer;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface ManufacturerService {
    List<Manufacturer> getAll(ManufacturerSearchParameters manufacturerSearchParameters);

    Manufacturer getById(Long id);

    Manufacturer getByName(String name);

    Manufacturer create(Manufacturer manufacturer);

    Manufacturer update(Manufacturer manufacturer);

    void delete(Long id);

    Page<Manufacturer> findPaginated(Pageable pageable);
}
