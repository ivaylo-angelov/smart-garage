package com.web.smartgarage.services.contracts;


import com.web.smartgarage.models.User;

public interface ResetPasswordService {

    void updateResetPasswordToken(String token, String email);

    User getUserByResetPasswordToken(String resetPasswordToken);

    void updatePassword(User user, String newPassword);
}
