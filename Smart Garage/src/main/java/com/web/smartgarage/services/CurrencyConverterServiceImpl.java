package com.web.smartgarage.services;


import com.web.smartgarage.services.contracts.CurrencyConverterService;
import com.web.smartgarage.models.Currency;
import com.web.smartgarage.models.Rate;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.Objects;
import java.util.Set;

@Service
public class CurrencyConverterServiceImpl implements CurrencyConverterService {
    private static final String URLCONVETER
            = "http://data.fixer.io/api/latest?access_key=7899dd9f51bf09100febe49ea4d9397c&" +
            "symbols=%s,%s&format=1";

    private static final String URLGETALLCURRENCIES =
            "http://data.fixer.io/api/symbols?access_key=7899dd9f51bf09100febe49ea4d9397c&";

    @Override
    public Set<String> getAllCurrencies() {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Currency> request = new HttpEntity<>(new Currency());
        ResponseEntity<Currency> response = restTemplate
                .exchange(URLGETALLCURRENCIES, HttpMethod.GET, request, Currency.class);
        return Objects.requireNonNull(response.getBody()).getSymbols().keySet();
    }

    @Override
    public Double convert(String from, String to, Double amount) {
        RestTemplate restTemplate = new RestTemplate();
        HttpEntity<Rate> request = new HttpEntity<>(new Rate());
        ResponseEntity<Rate> response = restTemplate
                .exchange(String.format(URLCONVETER, from, to), HttpMethod.GET, request, Rate.class);
        Rate rate = response.getBody();
        return roundAvoid(((amount / rate.getRates().get(from)) * rate.getRates().get(to)), 2);
    }

    private static double roundAvoid(double value, int places) {
        double scale = Math.pow(10, places);
        return Math.round(value * scale) / scale;
    }
}
