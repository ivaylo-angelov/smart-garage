package com.web.smartgarage.services.contracts;

import com.web.smartgarage.models.User;
import com.web.smartgarage.models.searchparameters.UserSearchParameters;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.userdetails.UserDetailsService;

import javax.mail.MessagingException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public interface UserService extends UserDetailsService {

    List<User> getAll(UserSearchParameters userSearchParameters);

    User getById(Long id);

    User getByEmail(String email);

    User getByPhone(String phone);

    User create(User user) throws UnsupportedEncodingException, MessagingException;

    User update(User user);

    Page<User> findPaginated(Pageable pageable, UserSearchParameters userSearchParameters);

    void delete(Long id);
}
