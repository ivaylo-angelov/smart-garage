package com.web.smartgarage.services.contracts;


import com.web.smartgarage.models.SmsRequest;

public interface SmsSender {

    void sendSms(SmsRequest smsRequest);

    // or maybe void sendSms(String phoneNumber, String message);
}
