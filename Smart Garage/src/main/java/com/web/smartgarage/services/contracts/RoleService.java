package com.web.smartgarage.services.contracts;


import com.web.smartgarage.models.Role;

public interface RoleService {
    Role getByName(String name);
}
