package com.web.smartgarage.controllers.rest;

import com.web.smartgarage.services.contracts.UserService;
import com.web.smartgarage.exceptions.DuplicateEntityException;
import com.web.smartgarage.exceptions.EntityNotFoundException;
import com.web.smartgarage.modelmapper.UserModelMapper;
import com.web.smartgarage.models.User;
import com.web.smartgarage.models.UserDto;
import com.web.smartgarage.models.searchparameters.UserSearchParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
@RequestMapping("api/employees")
public class EmployeeController {
    private final UserService service;
    private final UserModelMapper mapper;


    @Autowired
    public EmployeeController(UserService service, UserModelMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public List<User> getAll(@RequestParam(required = false) String email,
                             @RequestParam(required = false) String firstName,
                             @RequestParam(required = false) String lastName,
                             @RequestParam(required = false) String phone) {
        return service.getAll(new UserSearchParameters(email, firstName, lastName, phone, "administrator"));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public User getById(@PathVariable Long id) {
        return service.getById(id);
    }

    @GetMapping("/email/{email}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public User getByEmail(@PathVariable String email) {
        return service.getByEmail(email);
    }

    @GetMapping("/phone/{phone}")
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('ADMINISTRATOR')")
    public User getByPhone(@PathVariable String phone) {
        return service.getByPhone(phone);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public User create(@Valid @RequestBody UserDto userDto) {
        try {
            User user = mapper.fromDto(userDto);
            return service.create(user);
        } catch (EntityNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND, e.getMessage());
        } catch (DuplicateEntityException | UnsupportedEncodingException | MessagingException e) {
            throw new ResponseStatusException(HttpStatus.CONFLICT, e.getMessage());
        }

    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public User update(@PathVariable Long id, @Valid @RequestBody UserDto userDto) {
        User user = mapper.fromDto(userDto, id);
        return service.update(user);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}
