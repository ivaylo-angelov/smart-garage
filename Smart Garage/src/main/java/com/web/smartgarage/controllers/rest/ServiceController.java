package com.web.smartgarage.controllers.rest;

import com.web.smartgarage.exceptions.DuplicateEntityException;
import com.web.smartgarage.exceptions.EntityNotFoundException;
import com.web.smartgarage.modelmapper.ServiceModelMapper;
import com.web.smartgarage.models.Service;
import com.web.smartgarage.models.dto.ServiceDto;
import com.web.smartgarage.models.searchparameters.ServiceSearchParameters;
import com.web.smartgarage.services.contracts.ServiceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/services")
public class ServiceController {

    private final ServiceService service;
    private final ServiceModelMapper modelMapper;

    @Autowired
    public ServiceController(ServiceService service, ServiceModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CLIENT')")
    public List<Service> getAll(@RequestParam(required = false) String name,
                                @RequestParam(required = false) Double fromPrice,
                                @RequestParam(required = false) Double toPrice) {
        return service.getAll(new ServiceSearchParameters(name, fromPrice, toPrice));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CLIENT')")
    public Service getById(@Valid @PathVariable Long id) {
        return service.getById(id);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Service create(@RequestBody @Valid ServiceDto serviceDto) {
        Service createService = modelMapper.fromDto(serviceDto);
        return service.create(createService);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Service update(@Valid @PathVariable Long id, @Valid @RequestBody ServiceDto serviceDto) {
        Service updateService = modelMapper.fromDto(serviceDto, id);
        return service.update(updateService);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void delete(@Valid @PathVariable Long id) {
        service.delete(id);
    }
}
