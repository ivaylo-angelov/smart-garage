package com.web.smartgarage.controllers.mvc;

import com.web.smartgarage.exceptions.DuplicateEntityException;
import com.web.smartgarage.exceptions.EntityNotFoundException;
import com.web.smartgarage.modelmapper.UserModelMapper;
import com.web.smartgarage.models.User;
import com.web.smartgarage.models.UserDto;
import com.web.smartgarage.models.searchparameters.UserSearchParameters;
import com.web.smartgarage.services.contracts.RoleService;
import com.web.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/employees")
public class EmployeeMvcController {

    private final UserModelMapper mapper;
    private final UserService userService;
    private final RoleService roleService;

    @Autowired
    public EmployeeMvcController(UserModelMapper mapper,
                                 UserService userService,
                                 RoleService roleService) {
        this.mapper = mapper;
        this.userService = userService;
        this.roleService = roleService;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String showAllEmployees(Model model,
                                   @RequestParam("page") Optional<Integer> page,
                                   @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        UserSearchParameters usp = new UserSearchParameters();
        usp.setRole("administrator");

        if (!model.containsAttribute("employeeSearchParameters")) {
            model.addAttribute("employeeSearchParameters", usp);
        }

        Page<User> employeePage = userService.findPaginated(PageRequest.of(currentPage - 1, pageSize)
                , (UserSearchParameters) model.getAttribute("employeeSearchParameters"));
        List<User> userList = employeePage.getContent();

        model.addAttribute("employeeList", userList);
        model.addAttribute("employeePage", employeePage);

        int totalPages = employeePage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "employees";
    }

    @GetMapping("/reset")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String handleEmployeeReset(Model model) {
        return "redirect:/employees";
    }

    @PostMapping("/search")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String handleEmployeeSearch(Model model,
                                       @RequestParam("page") Optional<Integer> page,
                                       @RequestParam("size") Optional<Integer> size,
                                       @ModelAttribute("employeeSearchParameters") UserSearchParameters usp) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        usp.setRole("administrator");

        Page<User> employeePage = userService.findPaginated(PageRequest.of(currentPage - 1, pageSize),
                usp);
        List<User> userList = employeePage.getContent();

        model.addAttribute("employeeList", userList);
        model.addAttribute("employeePage", employeePage);
        model.addAttribute("employeeSearchParameters", usp);

        int totalPages = employeePage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "employees";
    }

    @GetMapping("/search")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String handleSearchGetEmployees(Model model) {
        return showAllEmployees(model, Optional.empty(), Optional.empty());
    }

    @GetMapping("/create")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String showCreateEmployeePage(Model model) {
        model.addAttribute("employee", new UserDto());
        return "employee-new";
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String handleCreateVisitPage(Model model,
                                        @Valid @ModelAttribute("employee") UserDto userDto,
                                        BindingResult errors) {
        try {
            User user = mapper.fromDto(userDto);
            user.setRoles(Set.of(roleService.getByName("ROLE_ADMINISTRATOR")));
            userService.create(user);
            return "redirect:/employees";
        } catch (DuplicateEntityException | MessagingException | UnsupportedEncodingException e) {
            if (e.getMessage().contains("confirmEmail")) {
                errors.rejectValue("userCredentials.confirmEmail", "email_error", e.getMessage());
            } else if (e.getMessage().contains("email")) {
                errors.rejectValue("userCredentials.email", "email_error", e.getMessage());
            } else {
                errors.rejectValue("phone", "phone_error", e.getMessage());
            }
            return "employee-new";
        }
    }

    @GetMapping("/{id}/update")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String showEditProfilePage(@PathVariable Long id, Model model) {
        User user;
        UserDto userDto;
        try {
            user = userService.getById(id);
            userDto = mapper.toDto(user);
        } catch (EntityNotFoundException e) {
            return "redirect:/employees";
        }
        model.addAttribute("employee", userDto);
        model.addAttribute("employeeId", user.getId());
        return "employee-update";
    }

    @PostMapping("/{id}/update")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String editEmployeeProfile(@Valid @ModelAttribute("employee") UserDto userDto,
                                      @PathVariable Long id,
                                      BindingResult errors) {
        try {
            User user = mapper.fromDto(userDto, id);
            userService.update(user);
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("email")) {
                errors.rejectValue("userCredentials.email", "email_error", e.getMessage());
            } else {
                errors.rejectValue("phone", "phone_error", e.getMessage());
            }
            return "employee-update";
        } catch (EntityNotFoundException e) {
            return "error404";
        }
        return "redirect:/employees";
    }

    @GetMapping("/{id}/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String deleteEmployee(@PathVariable Long id, Model model) {
        try {
            userService.delete(id);
            return "redirect:/employees";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error404";
        }
    }
}
