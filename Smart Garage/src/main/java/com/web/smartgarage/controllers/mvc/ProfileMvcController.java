package com.web.smartgarage.controllers.mvc;

import com.web.smartgarage.services.contracts.UserService;
import com.web.smartgarage.exceptions.DuplicateEntityException;
import com.web.smartgarage.exceptions.EntityNotFoundException;
import com.web.smartgarage.modelmapper.UserModelMapper;
import com.web.smartgarage.models.User;
import com.web.smartgarage.models.UserDto;
import com.web.smartgarage.models.searchparameters.VisitSearchParameters;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.validation.Valid;

@Controller
@RequestMapping("/profile")
public class ProfileMvcController {

    private final UserService userService;
    private final UserModelMapper userModelMapper;

    public ProfileMvcController(UserService userService, UserModelMapper userModelMapper) {
        this.userService = userService;
        this.userModelMapper = userModelMapper;
    }

    @GetMapping
    @PreAuthorize("isAuthenticated()")
    public String profile(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();
        User user = userService.getByEmail(currentPrincipalName);
        model.addAttribute("currentUser", user);
        model.addAttribute("visitSearchParameters", new VisitSearchParameters());
        return "profile";
    }

    @GetMapping("/update")
    @PreAuthorize("isAuthenticated()")
    public String showEditProfilePage(Model model) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        User userToUpdate = userService.getByEmail(currentPrincipalName);
        UserDto userDto = userModelMapper.toDto(userToUpdate);

        model.addAttribute("customer", userDto);
        return "profile-update";
    }

    @PostMapping("/update")
    public String handleEditProfile(@Valid @ModelAttribute("customer") UserDto userDto,
                                    BindingResult errors) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        String currentPrincipalName = authentication.getName();

        userDto.getUserCredentials().setConfirmEmail(currentPrincipalName);
        userDto.getUserCredentials().setEmail(currentPrincipalName);

        try {
            User user = userService.getByEmail(currentPrincipalName);
            User userToUpdate = userModelMapper.fromDto(userDto, user.getId());
            userService.update(userToUpdate);
        } catch (EntityNotFoundException | DuplicateEntityException e) {
            if (e.getMessage().contains("email")) {
                errors.rejectValue("userCredentials.email", "invalid_email", e.getMessage());
                return "profile-update";
            } else if (e.getMessage().contains("phone")) {
                errors.rejectValue("phone", "invalid_phone", e.getMessage());
                return "profile-update";
            }
        }
        return "redirect:/profile";
    }
}
