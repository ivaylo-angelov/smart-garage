package com.web.smartgarage.controllers.rest;

import com.web.smartgarage.modelmapper.ModelModelMapper;
import com.web.smartgarage.models.Model;
import com.web.smartgarage.models.dto.ModelDto;
import com.web.smartgarage.models.searchparameters.ModelSearchParameters;
import com.web.smartgarage.services.contracts.ModelService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/models")
public class ModelController {
    private final ModelService service;
    private final ModelModelMapper modelMapper;


    @Autowired
    public ModelController(ModelService service, ModelModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CLIENT')")
    public List<Model> getAll(String name,
                              String manufacturerName) {
        return service.getAll(new ModelSearchParameters(name, manufacturerName));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CLIENT')")
    public Model getById(@PathVariable Long id) {
        return service.getById(id);
    }

    @GetMapping("/name/{name}")
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CLIENT')")
    public Model getByName(@PathVariable String name) {
        return service.getByName(name);
    }


    @PostMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Model create(@Valid @RequestBody ModelDto modelDto) {
        Model model = modelMapper.fromDto(modelDto);
        return service.create(model);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Model update(@PathVariable Long id, @Valid @RequestBody ModelDto modelDto) {
        Model model = modelMapper.fromDto(modelDto, id);
        return service.update(model);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}
