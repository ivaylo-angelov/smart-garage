package com.web.smartgarage.controllers.rest;

import com.web.smartgarage.exceptions.DuplicateEntityException;
import com.web.smartgarage.exceptions.EntityNotFoundException;
import com.web.smartgarage.modelmapper.VehicleModelMapper;
import com.web.smartgarage.models.Vehicle;
import com.web.smartgarage.models.dto.VehicleDto;
import com.web.smartgarage.models.searchparameters.VehicleSearchParameters;
import com.web.smartgarage.services.contracts.VehicleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.server.ResponseStatusException;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/api/vehicles")
public class VehicleController {

    private final VehicleService service;
    private final VehicleModelMapper modelMapper;

    @Autowired
    public VehicleController(VehicleService service, VehicleModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public List<Vehicle> getAll(@RequestParam(required = false) String email,
                                @RequestParam(required = false) String licensePlate,
                                @RequestParam(required = false) String vin,
                                @RequestParam(required = false) String phone) {
        return service.getAll(new VehicleSearchParameters(email, licensePlate, vin, phone));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CLIENT')")
    public Vehicle getById(@Valid @PathVariable Long id) {
        return service.getById(id);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Vehicle create(@RequestBody @Valid VehicleDto vehicleDto) {
        Vehicle vehicle = modelMapper.fromDto(vehicleDto);
        return service.create(vehicle);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Vehicle update(@Valid @PathVariable Long id, @Valid @RequestBody VehicleDto vehicleDto) {
        Vehicle vehicle = modelMapper.fromDto(vehicleDto, id);
        return service.update(vehicle);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void delete(@Valid @PathVariable Long id) {
        service.delete(id);
    }
}
