package com.web.smartgarage.controllers.mvc;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class MainController {

    @GetMapping("/")
    public String root() {
        return "index";
    }

    @GetMapping("/login")
    public String login(Model model) {
        return "login";
    }

    @GetMapping("/activities")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String activities(Model model) {
        return "activities";
    }


    @GetMapping("/contact")
    public String showContactPage() {
        return "contact";
    }

}
