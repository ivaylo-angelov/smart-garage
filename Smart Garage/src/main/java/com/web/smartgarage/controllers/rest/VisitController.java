package com.web.smartgarage.controllers.rest;

import com.web.smartgarage.modelmapper.VisitModelMapper;
import com.web.smartgarage.models.Service;
import com.web.smartgarage.models.Visit;
import com.web.smartgarage.models.dto.VisitDto;
import com.web.smartgarage.models.searchparameters.VisitSearchParameters;
import com.web.smartgarage.services.contracts.ServiceService;
import com.web.smartgarage.services.contracts.VisitService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.time.LocalDate;
import java.util.List;

@RestController
@RequestMapping("/api/visits")
public class VisitController {

    private final VisitService service;
    private final ServiceService serviceService;
    private final VisitModelMapper modelMapper;

    @Autowired
    public VisitController(VisitService service,
                           ServiceService serviceService,
                           VisitModelMapper modelMapper) {
        this.service = service;
        this.serviceService = serviceService;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public List<Visit> getAll(@RequestParam(required = false)
                              @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
                              @RequestParam(required = false) String licensePlate,
                              @RequestParam(required = false) String customerEmail,
                              @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate untilDate) {
        return service.getAll(new VisitSearchParameters(date, untilDate, licensePlate, customerEmail));
    }

    @GetMapping("/customer")
    @PreAuthorize("hasRole('CLIENT')")
    public List<Visit> forCustomerGetAll(@RequestParam(required = false)
                                         @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate date,
                                         @RequestParam(required = false) String licensePlate,
                                         @RequestParam String customerEmail,
                                         @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) LocalDate untilDate) {
        return service.getAll(new VisitSearchParameters(date, untilDate, licensePlate, customerEmail));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CLIENT')")
    public Visit getById(@Valid @PathVariable Long id) {
        return service.getById(id);
    }

    @GetMapping("/visit-services/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CLIENT')")
    public List<Service> getVisitServices(@PathVariable Long id) {
        Visit visit = getById(id);
        return serviceService.getServicesByVisit(visit);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Visit create(@Valid @RequestBody VisitDto visitDto) {
        Visit visit = modelMapper.fromDto(visitDto);
        return service.create(visit);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Visit update(@Valid @PathVariable Long id, @Valid @RequestBody VisitDto visitDto) throws MessagingException {
        Visit visit = modelMapper.fromDto(visitDto, id);
        return service.update(visit);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void delete(@Valid @PathVariable Long id) {
        service.delete(id);
    }

}
