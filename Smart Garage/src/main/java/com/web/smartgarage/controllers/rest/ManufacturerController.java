package com.web.smartgarage.controllers.rest;

import com.web.smartgarage.modelmapper.ManufacturerModelMapper;
import com.web.smartgarage.models.Manufacturer;
import com.web.smartgarage.models.dto.ManufacturerDto;
import com.web.smartgarage.models.searchparameters.ManufacturerSearchParameters;
import com.web.smartgarage.services.contracts.ManufacturerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("api/manufacturers")
public class ManufacturerController {

    private final ManufacturerService service;
    private final ManufacturerModelMapper modelMapper;


    @Autowired
    public ManufacturerController(ManufacturerService service, ManufacturerModelMapper modelMapper) {
        this.service = service;
        this.modelMapper = modelMapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CLIENT')")
    public List<Manufacturer> getAll(String name) {
        return service.getAll(new ManufacturerSearchParameters(name));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CLIENT')")
    public Manufacturer getById(@PathVariable Long id) {
        return service.getById(id);
    }

    @GetMapping("/name/{name}")
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CLIENT')")
    public Manufacturer getByName(@PathVariable String name) {
        return service.getByName(name);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Manufacturer create(@Valid @RequestBody ManufacturerDto manufacturerDto) {
        Manufacturer manufacturer = modelMapper.fromDto(manufacturerDto);
        return service.create(manufacturer);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public Manufacturer update(@PathVariable Long id, @Valid @RequestBody ManufacturerDto manufacturerDto) {
        Manufacturer manufacturer = modelMapper.fromDto(manufacturerDto, id);
        return service.update(manufacturer);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}
