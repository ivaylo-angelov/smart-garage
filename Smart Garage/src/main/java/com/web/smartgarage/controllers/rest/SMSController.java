package com.web.smartgarage.controllers.rest;

import com.web.smartgarage.models.SmsRequest;
import com.web.smartgarage.services.SmsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.validation.Valid;

@RestController
@RequestMapping("api/v1/sms")
public class SMSController {

    private final com.web.smartgarage.services.SmsService SMSService;

    @Autowired
    public SMSController(SmsService SMSService) {
        this.SMSService = SMSService;
    }

    @PostMapping
    public void sendSms(@Valid @RequestBody SmsRequest smsRequest) {
        SMSService.sendSms(smsRequest);
    }
}
