package com.web.smartgarage.controllers.mvc;

import com.web.smartgarage.services.contracts.UserService;
import com.web.smartgarage.exceptions.DeletingEntityException;
import com.web.smartgarage.exceptions.DuplicateEntityException;
import com.web.smartgarage.exceptions.EntityNotFoundException;
import com.web.smartgarage.modelmapper.UserModelMapper;
import com.web.smartgarage.models.User;
import com.web.smartgarage.models.UserDto;
import com.web.smartgarage.models.searchparameters.ModelSearchParameters;
import com.web.smartgarage.models.searchparameters.UserSearchParameters;
import com.web.smartgarage.services.contracts.ModelService;
import com.web.smartgarage.services.contracts.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/customers")
public class CustomerMvcController {

    private final UserModelMapper mapper;
    private final UserService userService;
    private final ModelService modelService;
    private final RoleService roleService;

    @Autowired
    public CustomerMvcController(UserModelMapper mapper,
                                 UserService userService,
                                 ModelService modelService,
                                 RoleService roleService) {
        this.mapper = mapper;
        this.userService = userService;
        this.modelService = modelService;
        this.roleService = roleService;
    }

    @ModelAttribute("models")
    public List<com.web.smartgarage.models.Model> models() {
        return modelService
                .getAll(new ModelSearchParameters())
                .stream()
                .sorted(Comparator.comparing(m -> m.getManufacturer().getName()))
                .collect(Collectors.toList());
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String showAllCustomers(Model model,
                                   @RequestParam("page") Optional<Integer> page,
                                   @RequestParam("size") Optional<Integer> size) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        UserSearchParameters usp = new UserSearchParameters();
        usp.setRole("client");
        Page<User> customerPage = userService.findPaginated(PageRequest.of(currentPage - 1, pageSize),
                usp);
        List<User> userList = customerPage.getContent();

        model.addAttribute("customerList", userList);
        model.addAttribute("customerPage", customerPage);
        model.addAttribute("customerSearchParameters", usp);

        int totalPages = customerPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "customers";
    }

    @GetMapping("/reset")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String handleCustomerReset(Model model) {
        return "redirect:/customers";
    }

    @GetMapping("/search")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String handleSearchCustomer(Model model) {
        return showAllCustomers(model, Optional.empty(), Optional.empty());
    }

    @PostMapping("/search")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String handleCustomerSearch(Model model,
                                       @RequestParam("page") Optional<Integer> page,
                                       @RequestParam("size") Optional<Integer> size,
                                       @ModelAttribute("customerSearchParameters") UserSearchParameters userSearchParameters) {
        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        userSearchParameters.setRole("client");

        Page<User> customerPage = userService.findPaginated(PageRequest.of(currentPage - 1, pageSize),
                userSearchParameters);
        List<User> customerList = customerPage.getContent();

        model.addAttribute("customerList", customerList);
        model.addAttribute("customerPage", customerPage);

        int totalPages = customerPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        return "customers";
    }

    @GetMapping("/create")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String showCreateCustomerPage(Model model) {
        model.addAttribute("customer", new UserDto());
        return "customer-new";
    }

    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String handleCreateVisitPage(Model model,
                                        @ModelAttribute("customer") @Valid UserDto userDto,
                                        BindingResult errors) {
        try {
            User user = mapper.fromDto(userDto);
            user.setRoles(Set.of(roleService.getByName("ROLE_CLIENT")));
            userService.create(user);
            return "redirect:/customers";
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("email")) {
                errors.rejectValue("userCredentials.email", "email_error", e.getMessage());
            } else {
                errors.rejectValue("phone", "phone_error", e.getMessage());
            }
            return "customer-new";
        } catch (UnsupportedEncodingException | MessagingException e1) {
            return "error404";
        }
    }

    @GetMapping("/{id}/update")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String showEditProfilePage(@PathVariable Long id, Model model) {
        User user;
        UserDto userDto;
        try {
            user = userService.getById(id);
            userDto = mapper.toDto(user);
        } catch (EntityNotFoundException e) {
            return "redirect:/customers";
        }
        model.addAttribute("customer", userDto);
        model.addAttribute("customerId", user.getId());
        return "customer-update";
    }

    @PostMapping("/{id}/update")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String editCustomerProfile(@ModelAttribute("customer") UserDto userDto,
                                      @Valid @PathVariable Long id,
                                      BindingResult errors) {
        try {
            User user = mapper.fromDto(userDto, id);
            userService.update(user);
        } catch (DuplicateEntityException e) {
            if (e.getMessage().contains("phone")) {
                errors.rejectValue("phone", "duplicate_phone", e.getMessage());
            } else {
                errors.rejectValue("userCredentials.email", "duplicate_email", e.getMessage());
            }
            return "customer-update";
        } catch (EntityNotFoundException e) {
            errors.rejectValue("licensePlate", "license_plate", e.getMessage());
            return "customer-update";
        }
        return "redirect:/customers";
    }

    @GetMapping("/{id}/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String deleteCustomer(@PathVariable Long id, Model model) {
        try {
            userService.delete(id);
            return "redirect:/customers";
        } catch (EntityNotFoundException | DeletingEntityException e) {
            model.addAttribute("error", e.getMessage());
            return "error404";
        }
    }
}
