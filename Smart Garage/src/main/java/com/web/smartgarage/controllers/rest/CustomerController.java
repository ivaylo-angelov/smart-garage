package com.web.smartgarage.controllers.rest;

import com.web.smartgarage.modelmapper.UserModelMapper;
import com.web.smartgarage.models.User;
import com.web.smartgarage.models.UserDto;
import com.web.smartgarage.models.searchparameters.UserSearchParameters;
import com.web.smartgarage.services.contracts.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.mail.MessagingException;
import javax.validation.Valid;
import java.io.UnsupportedEncodingException;
import java.util.List;

@RestController
@RequestMapping("api/customers")
public class CustomerController {
    private final UserService service;
    private final UserModelMapper mapper;

    @Autowired
    public CustomerController(UserService service, UserModelMapper mapper) {
        this.service = service;
        this.mapper = mapper;
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public List<User> getAll(@RequestParam(required = false) String email,
                             @RequestParam(required = false) String firstName,
                             @RequestParam(required = false) String lastName,
                             @RequestParam(required = false) String phone) {
        return service.getAll(new UserSearchParameters(email,
                firstName,
                lastName,
                phone,
                "client"));
    }

    @GetMapping("/numberOfCustomers")
    public Integer getNumberOfCustomers() {
        return service.getAll(new UserSearchParameters()).size();
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('CLIENT') or hasRole('ADMINISTRATOR')")
    public User getById(@PathVariable Long id) {
        return service.getById(id);
    }

    @GetMapping("/email/{email}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public User getByEmail(@PathVariable String email) {
        return service.getByEmail(email);
    }

    @GetMapping("/phone/{phone}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public User getByUsername(@PathVariable String phone) {
        return service.getByPhone(phone);
    }

    @PostMapping
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('ADMINISTRATOR')")
    public User create(@Valid @RequestBody UserDto userDto) throws MessagingException, UnsupportedEncodingException {
        User user = mapper.fromDto(userDto);
        return service.create(user);
    }

    @PutMapping("/{id}")
    @PreAuthorize("hasRole('CLIENT') or hasRole('ADMINISTRATOR')")
    public User update(@PathVariable Long id, @Valid @RequestBody UserDto userDto) {
        User user = mapper.fromDto(userDto, id);
        return service.update(user);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR') or hasRole('CLIENT') or hasRole('ADMINISTRATOR')")
    public void delete(@PathVariable Long id) {
        service.delete(id);
    }
}
