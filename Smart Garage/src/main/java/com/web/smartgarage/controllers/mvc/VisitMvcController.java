package com.web.smartgarage.controllers.mvc;

import com.itextpdf.html2pdf.ConverterProperties;
import com.itextpdf.html2pdf.HtmlConverter;
import com.web.smartgarage.models.*;
import com.web.smartgarage.repositories.contracts.VisitRepository;
import com.web.smartgarage.services.contracts.*;
import com.web.smartgarage.exceptions.DuplicateEntityException;
import com.web.smartgarage.exceptions.EntityNotFoundException;
import com.web.smartgarage.exceptions.LocalDateException;
import com.web.smartgarage.modelmapper.VisitModelMapper;
import com.web.smartgarage.models.dto.CurrencyDto;
import com.web.smartgarage.models.dto.VisitDto;
import com.web.smartgarage.models.searchparameters.ServiceSearchParameters;
import com.web.smartgarage.models.searchparameters.VehicleSearchParameters;
import com.web.smartgarage.models.searchparameters.VisitSearchParameters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.WebContext;

import javax.mail.MessagingException;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/visits")
public class VisitMvcController {

    private final VisitService visitService;
    private final ServiceService serviceService;
    private final VisitModelMapper modelMapper;
    private final VehicleService vehicleService;
    private final CurrencyConverterService currencyConverterService;
    private final ServletContext servletContext;
    private final TemplateEngine templateEngine;
    private final MailSender mailSender;
    private final UserService userService;
    private final VisitRepository visitRepository;

    private final static String DEFAULT_CURRENCY = "BGN";

    @Autowired
    public VisitMvcController(VisitService visitService,
                              ServiceService serviceService,
                              VisitModelMapper modelMapper,
                              VehicleService vehicleService,
                              CurrencyConverterService currencyConverter,
                              ServletContext servletContext,
                              TemplateEngine templateEngine,
                              MailSender mailSender,
                              UserService userService, VisitRepository visitRepository) {
        this.visitService = visitService;
        this.serviceService = serviceService;
        this.modelMapper = modelMapper;
        this.vehicleService = vehicleService;
        this.servletContext = servletContext;
        this.templateEngine = templateEngine;
        this.currencyConverterService = currencyConverter;
        this.mailSender = mailSender;
        this.userService = userService;
        this.visitRepository = visitRepository;
    }

    @ModelAttribute("currencyList")
    public Set<String> getAllCurrencies() {
        return currencyConverterService.getAllCurrencies();
    }

    @ModelAttribute("currencyDto")
    public CurrencyDto setCurrency() {
        return new CurrencyDto(DEFAULT_CURRENCY);
    }

    @ModelAttribute("vehicles")
    public List<Vehicle> vehicles() {
        return vehicleService.getAll(new VehicleSearchParameters());
    }

    @ModelAttribute("serviceList")
    public List<Service> services() {
        return serviceService.getAll(new ServiceSearchParameters());
    }

    @GetMapping
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String showAllVisits(Model model,
                                @RequestParam("page") Optional<Integer> page,
                                @RequestParam("size") Optional<Integer> size) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Visit> visitPage = visitService.findPaginated(PageRequest.of(currentPage - 1, pageSize),
                new VisitSearchParameters());
        List<Visit> visitList = visitPage.getContent();

        model.addAttribute("visitList", visitList);
        model.addAttribute("visitSearchParameters", new VisitSearchParameters());
        model.addAttribute("visitPage", visitPage);

        int totalPages = visitPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }

        return "visits";
    }

    @PreAuthorize("hasRole('CLIENT')")
    @GetMapping("/my-visits")
    public String getCustomerVisits(@RequestParam("page") Optional<Integer> page,
                                    @RequestParam("size") Optional<Integer> size, Model model) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String currentPrincipalName = authentication.getName();
            VisitSearchParameters visitSearchParameters = new VisitSearchParameters();
            visitSearchParameters.setCustomerEmail(currentPrincipalName);
            int currentPage = page.orElse(1);
            int pageSize = size.orElse(5);

            Page<Visit> visitPage = visitService.findPaginated(PageRequest.of(currentPage - 1, pageSize),
                    visitSearchParameters);
            List<Visit> visitList = visitPage.getContent();

            model.addAttribute("visitList", visitList);
            model.addAttribute("visitSearchParameters", new VisitSearchParameters());
            model.addAttribute("visitPage", visitPage);

            int totalPages = visitPage.getTotalPages();
            if (totalPages > 0) {
                List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                        .boxed()
                        .collect(Collectors.toList());
                model.addAttribute("pageNumbers", pageNumbers);
            }
            return "customer-visits";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error404";
        }
    }

    @GetMapping("/clients/search")
    @PreAuthorize("hasRole('CLIENT') or hasRole('ADMINISTRATOR')")
    public String handleClientSearchVisits(Model model) {
        return showAllVisits(model, Optional.empty(), Optional.empty());
    }

    @PostMapping("/clients/search")
    public String handleClientVisitSearch(Model model, @RequestParam("page") Optional<Integer> page,
                                    @RequestParam("size") Optional<Integer> size,
                                    @ModelAttribute("visitSearchParameters") VisitSearchParameters visitSearchParameters) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Visit> visitPage = visitService.findPaginated(PageRequest.of(currentPage - 1, pageSize),
                visitSearchParameters);
        List<Visit> visitList = visitPage.getContent();

        model.addAttribute("visitPage", visitPage);

        int totalPages = visitPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        model.addAttribute("visitList", visitList);

        return "customer-visits";
    }

    @GetMapping("/search")
    @PreAuthorize("hasRole('CLIENT') or hasRole('ADMINISTRATOR')")
    public String handleSearchGetVisits(Model model) {
        return showAllVisits(model, Optional.empty(), Optional.empty());
    }

    @PostMapping("/search")
    public String handleVisitSearch(Model model, @RequestParam("page") Optional<Integer> page,
                                    @RequestParam("size") Optional<Integer> size,
                                    @ModelAttribute("visitSearchParameters") VisitSearchParameters visitSearchParameters) {

        int currentPage = page.orElse(1);
        int pageSize = size.orElse(5);

        Page<Visit> visitPage = visitService.findPaginated(PageRequest.of(currentPage - 1, pageSize),
                visitSearchParameters);
        List<Visit> visitList = visitPage.getContent();

        model.addAttribute("visitPage", visitPage);

        int totalPages = visitPage.getTotalPages();
        if (totalPages > 0) {
            List<Integer> pageNumbers = IntStream.rangeClosed(1, totalPages)
                    .boxed()
                    .collect(Collectors.toList());
            model.addAttribute("pageNumbers", pageNumbers);
        }
        model.addAttribute("visitList", visitList);

        return "visits";
    }

    @PostMapping("/apply/currency/{id}")
    public String applyCurrency(@ModelAttribute("currencyDto") CurrencyDto currency,
                                @PathVariable Long id, Model model) {
        Visit visit = visitService.getById(id);
        visit.setTotalPrice(currencyConverterService.convert("BGN", currency.getName(),
                visit.getTotalPrice()));
        List<Service> services = serviceService.getServicesByVisit(visit);
        services.forEach(e -> e.setPrice(currencyConverterService.convert("BGN", currency.getName(),
                e.getPrice())));
        model.addAttribute("visit", visit);
        model.addAttribute("services", services);
        return "visit";
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String showSingleVisit(@PathVariable Long id, Model model) {
        try {
            Visit visit = visitService.getById(id);
            List<Service> services = serviceService.getServicesByVisit(visit);
            model.addAttribute("visit", visit);
            model.addAttribute("services", services);
        } catch (EntityNotFoundException e) {
            return "redirect:/visits";
        }

        return "visit";
    }

    @GetMapping("/clients/{id}")
    @PreAuthorize("hasRole('CLIENT') or hasRole('ADMINISTRATOR')")
    public String showSingleClientVisit(@PathVariable Long id, Model model) {
        try {
            Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
            String currentPrincipalName = authentication.getName();
            Visit visit = visitService.getById(id);
            if (!visit.getVehicle().getUser().getUserCredentials().getEmail().equals(currentPrincipalName)) {
                model.addAttribute("error", "You are not allowed here!");
                return "error";
            }
            List<Service> services = serviceService.getServicesByVisit(visit);
            model.addAttribute("visit", visit);
            model.addAttribute("services", services);
        } catch (EntityNotFoundException e) {
            return "redirect:/visits";
        }

        return "visit";
    }

    @GetMapping("/create")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String showCreateVisitPage(Model model) {
        model.addAttribute("visit", new VisitDto());
        return "visit-new";
    }


    @PostMapping("/create")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String handleCreateVisitPage(@Valid @ModelAttribute("visit") VisitDto visitDto,
                                        BindingResult errors) {

        if (errors.hasErrors()) {
            return "visit-new";
        }

        try {
            Visit visit = modelMapper.fromDto(visitDto);
            visitService.create(visit);
            return "redirect:/visits";
        } catch (LocalDateException e) {
            errors.rejectValue("date", "invalid_date", e.getMessage());
            return "visit-new";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("licensePlate", "duplicate_licensePlate", e.getMessage());
            return "visit-new";
        } catch (EntityNotFoundException e) {
            errors.rejectValue("licensePlate", "invalid_licensePlate", e.getMessage());
            return "visit-new";
        }
    }

    @GetMapping("/{id}/update")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String showEditVisit(@PathVariable Long id, Model model) {
        try {
            Visit visit = visitService.getById(id);
            VisitDto visitDto = modelMapper.toDto(visit);
            model.addAttribute("visit", visitDto);
            return "visit-update";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error404";
        }
    }

    @PostMapping("/{id}/update")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String editVisit(@PathVariable Long id,
                            @Valid @ModelAttribute("visit") VisitDto visitDto,
                            BindingResult errors) {

        if (errors.hasErrors()) {
            return "visit-update";
        }

        try {
            Visit visit = modelMapper.fromDto(visitDto, id);
            visitService.update(visit);
            return "redirect:/visits";
        } catch (LocalDateException e) {
            errors.rejectValue("date", "Invalid_date", e.getMessage());
            return "visit-update";
        } catch (MessagingException e) {
            return "error404";
        } catch (EntityNotFoundException e) {
            errors.rejectValue("licensePlate", "invalid_licensePlate", e.getMessage());
            return "visit-update";
        } catch (DuplicateEntityException e) {
            errors.rejectValue("licensePlate", "Invalid_licensePlate", e.getMessage());
            return "visit-update";
        }
    }

    @RequestMapping(path = "/pdf/{id}/{currency}")
    @PreAuthorize("hasRole('CLIENT') or hasRole('ADMINISTRATOR')")
    public String exportToPdf(@PathVariable Long id,
                              @PathVariable String currency,
                              HttpServletRequest request,
                              HttpServletResponse response,
                              Model model) throws IOException, MessagingException {

        User currentUser = userService.getByEmail(SecurityContextHolder.getContext().getAuthentication().getName());
        Visit visit = visitService.getById(id);
        List<Service> services = serviceService.getServicesByVisit(visit);
        services.forEach(e -> e.setPrice(currencyConverterService.convert("BGN", currency,
                e.getPrice())));
        visit.setTotalPrice(currencyConverterService.convert("BGN", currency, visit.getTotalPrice()));

        WebContext context = new WebContext(request, response, servletContext);
        context.setVariable("visitEntity", visit);
        context.setVariable("services", services);
        context.setVariable("currency", currency);
        String orderHtml = templateEngine.process("invoice", context);

        ByteArrayOutputStream target = new ByteArrayOutputStream();
        ConverterProperties converterProperties = new ConverterProperties();
        converterProperties.setBaseUri("http://localhost:8080");

        HtmlConverter.convertToPdf(orderHtml, target, converterProperties);

        if (currentUser.isAdmin()) {
            visit.setStatus(VisitStatus.FINISHED);
            visitRepository.update(visit);
        }

        byte[] bytes = target.toByteArray();
        writeBytesToFile("C:\\DIPLOMNA_IVAYLO_ANGELOV\\Smart Garage\\invoices\\invoice.pdf", bytes);

        mailSender.sendEmailWithAttachment(visit.getVehicle().getUser().getUserCredentials().getEmail(),
                "Hello, this is your invoice",
                "Invoice", "" + visit.getId());

       return showSingleVisit(visit.getId(), model);
    }

    @GetMapping("/{id}/delete")
    @PreAuthorize("hasRole('ADMINISTRATOR')")
    public String deleteVisit(@PathVariable Long id, Model model) {
        try {
            visitService.delete(id);
            return "redirect:/visits";
        } catch (EntityNotFoundException e) {
            model.addAttribute("error", e.getMessage());
            return "error404";
        }
    }

    private static void writeBytesToFile(String fileOutput, byte[] bytes)
            throws IOException {

        try (FileOutputStream fos = new FileOutputStream(fileOutput)) {
            fos.write(bytes);
        }

    }
}
