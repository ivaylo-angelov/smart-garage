package com.web.smartgarage.repositories;

import com.web.smartgarage.models.VisitStatus;
import com.web.smartgarage.repositories.contracts.StatusRepository;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class StatusRepositoryImpl extends AbstractGenericGetRepository<VisitStatus> implements StatusRepository {

    @Autowired
    public StatusRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
    }

    @Override
    public List<VisitStatus> getAll() {
        return super.getAll(VisitStatus.class);
    }

    @Override
    public VisitStatus getById(Long id) {
        return super.getByField("id", id, VisitStatus.class);
    }
}
