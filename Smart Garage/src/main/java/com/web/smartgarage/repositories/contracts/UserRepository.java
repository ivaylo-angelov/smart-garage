package com.web.smartgarage.repositories.contracts;

import com.web.smartgarage.models.User;
import com.web.smartgarage.models.searchparameters.UserSearchParameters;

import java.util.List;

public interface UserRepository {

    List<User> getAll(UserSearchParameters userSearchParameters);

    User getById(Long id);

    User getByEmail(String email);

    User findByResetPassword(String email);

    User getByPhone(String phone);

    User create(User user);

    User update(User user);

    void delete(Long id);

}
