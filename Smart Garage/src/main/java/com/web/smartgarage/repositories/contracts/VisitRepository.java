package com.web.smartgarage.repositories.contracts;

import com.web.smartgarage.models.Visit;
import com.web.smartgarage.models.searchparameters.VisitSearchParameters;

import java.time.LocalDate;
import java.util.List;

public interface VisitRepository {

    List<Visit> getAll(VisitSearchParameters vsp);

    Visit getById(Long id);

    Visit getByDate(LocalDate date);

    Visit create(Visit visit);

    Visit update(Visit visit);

    void delete(Long id);
}
