package com.web.smartgarage.repositories.contracts;

import com.web.smartgarage.models.Role;

public interface RoleRepository {
    Role getById(Long id);

    Role getByName(String name);
}
