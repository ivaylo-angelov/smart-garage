package com.web.smartgarage.repositories.contracts;

import com.web.smartgarage.models.VisitStatus;

import java.util.List;

public interface StatusRepository {

    List<VisitStatus> getAll();

    VisitStatus getById(Long id);
}
