package com.web.smartgarage.repositories;

import com.web.smartgarage.models.searchparameters.UserSearchParameters;
import com.web.smartgarage.models.User;
import com.web.smartgarage.repositories.contracts.UserRepository;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UserRepositoryImpl extends AbstractGenericCrudRepository<User> implements UserRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public UserRepositoryImpl(SessionFactory sessionFactory) {
        super(sessionFactory);
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<User> getAll(UserSearchParameters userSearchParameters) {
        try (Session session = sessionFactory.openSession()) {
            Query<User> query = session.createNativeQuery("select u.user_id, u.first_name, u.last_name, u.phone, uc.email, uc.users_credentials_id, r.role_id " +
                    "from user_details as u " +
                    "join users_roles ur on u.user_id = ur.user_id " +
                    "join roles r on r.role_id = ur.role_id " +
                    "join users_credentials uc on uc.users_credentials_id = u.users_credentials_id " +
                    "where r.name like concat('%', :role, '%') " +
                    "and uc.email like concat('%', :email, '%') " +
                    "and u.first_name like concat('%', :firstName, '%') " +
                    "and u.last_name like concat('%', :lastName, '%') " +
                    "and u.phone like concat('%', :phone, '%')", User.class);
            query.setParameter("role", userSearchParameters.getRole());
            query.setParameter("email", userSearchParameters.getEmail() == null ? "" : userSearchParameters.getEmail());
            query.setParameter("firstName", userSearchParameters.getFirstName() == null ? "" : userSearchParameters.getFirstName());
            query.setParameter("lastName", userSearchParameters.getLastName() == null ? "" : userSearchParameters.getLastName());
            query.setParameter("phone", userSearchParameters.getPhone() == null ? "" : userSearchParameters.getPhone());
            return query.list();
        }
    }

    @Override
    public User getById(Long id) {
        return super.getByField("id", id, User.class);
    }

    @Override
    public User getByEmail(String email) {
        return super.getByField("userCredentials.email", email, User.class);
    }

    @Override
    public User findByResetPassword(String token) {
        return super.getByField("userCredentials.resetPassword", token, User.class);
    }

    @Override
    public User getByPhone(String phone) {
        return super.getByField("phone", phone, User.class);
    }

    @Override
    public User update(User user) {
        return super.update(user);
    }

    @Override
    public User create(User user) {
        return super.create(user);
    }

    @Override
    public void delete(Long id) {
        super.delete(id, User.class);
    }
}
