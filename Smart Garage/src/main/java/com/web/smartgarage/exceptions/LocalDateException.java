package com.web.smartgarage.exceptions;

public class LocalDateException extends RuntimeException {
    public LocalDateException(String message) {
        super(message);
    }
}
