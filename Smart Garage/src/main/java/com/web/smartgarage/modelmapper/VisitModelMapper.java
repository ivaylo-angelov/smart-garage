package com.web.smartgarage.modelmapper;

import com.web.smartgarage.models.dto.VisitDto;
import com.web.smartgarage.services.contracts.ServiceService;
import com.web.smartgarage.models.Service;
import com.web.smartgarage.models.Vehicle;
import com.web.smartgarage.models.Visit;
import com.web.smartgarage.repositories.contracts.ServiceRepository;
import com.web.smartgarage.repositories.contracts.VehicleRepository;
import com.web.smartgarage.repositories.contracts.VisitRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.HashSet;
import java.util.Set;

@Component
public class VisitModelMapper {

    private final VisitRepository repository;
    private final VehicleRepository vehicleRepository;
    private final ServiceRepository serviceRepository;
    private final ServiceService serviceService;

    @Autowired
    public VisitModelMapper(VisitRepository repository,
                            VehicleRepository vehicleRepository,
                            ServiceRepository serviceRepository,
                            ServiceService serviceService) {
        this.repository = repository;
        this.vehicleRepository = vehicleRepository;
        this.serviceRepository = serviceRepository;
        this.serviceService = serviceService;
    }

    public VisitDto toDto(Visit visit) {
        VisitDto visitDto = new VisitDto();
        visitDto.setDate(visit.getDate());
        visitDto.setStatus(visit.getStatus());
        visitDto.setLicensePlate(visit.getVehicle().getLicensePlate());

        Set<Long> services = new HashSet<>();

        for (Service service : serviceService.getServicesByVisit(visit)) {
            services.add(service.getId());
        }

        visitDto.setServices(services);
        return visitDto;
    }

    public Visit fromDto(VisitDto visitDto) {
        Visit visit = new Visit();
        dtoToObject(visitDto, visit);
        return visit;
    }

    public Visit fromDto(VisitDto visitDto, Long id) {
        Visit visit = repository.getById(id);
        dtoToObject(visitDto, visit);
        return visit;
    }

    private void dtoToObject(VisitDto visitDto, Visit visit) {
        Vehicle vehicle = vehicleRepository.getByLicensePlate(visitDto.getLicensePlate());

        visit.setDate(visitDto.getDate());
        visit.setVehicle(vehicle);
        visit.setStatus(visitDto.getStatus());
        visit.setTotalPrice(0.0);
        Set<Service> services = new HashSet<>();

        for (Long v : visitDto.getServices()) {
            services.add(serviceRepository.getById(v));
        }

        visit.setServices(services);
    }

}
