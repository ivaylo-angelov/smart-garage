package com.web.smartgarage.modelmapper;

import com.web.smartgarage.models.dto.VehicleDto;
import com.web.smartgarage.services.contracts.UserService;
import com.web.smartgarage.models.Model;
import com.web.smartgarage.models.User;
import com.web.smartgarage.models.Vehicle;
import com.web.smartgarage.models.VehicleType;
import com.web.smartgarage.repositories.contracts.ModelRepository;
import com.web.smartgarage.repositories.contracts.VehicleRepository;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class VehicleModelMapper {

    private final UserService userService;
    private final VehicleRepository repository;
    private final ModelRepository modelRepository;

    public VehicleModelMapper(UserService userService, VehicleRepository repository, ModelRepository modelRepository) {
        this.userService = userService;
        this.repository = repository;
        this.modelRepository = modelRepository;
    }

    public VehicleDto toDto(Vehicle vehicle) {
        VehicleDto vehicleDto = new VehicleDto();
        vehicleDto.setLicensePlate(vehicle.getLicensePlate());
        vehicleDto.setVin(vehicle.getVin());
        vehicleDto.setYear(vehicle.getYear());
        vehicleDto.setModelId(vehicle.getModel().getId());
        vehicleDto.setType(vehicle.getVehicleType().getValue());
        vehicleDto.setEmail(vehicle.getUser().getUserCredentials().getEmail());
        return vehicleDto;
    }

    public Vehicle fromDto(VehicleDto vehicleDto) {
        Vehicle vehicle = new Vehicle();
        dtoToObject(vehicleDto, vehicle);
        return vehicle;
    }

    public Vehicle fromDto(VehicleDto vehicleDto, Long id) {
        Vehicle vehicle = repository.getById(id);
        dtoToObject(vehicleDto, vehicle);
        return vehicle;
    }

    private void dtoToObject(VehicleDto vehicleDto, Vehicle vehicle) {
        Model model = modelRepository.getById(vehicleDto.getModelId());
        User user = userService.getByEmail(vehicleDto.getEmail());
        vehicle.setUser(user);
        vehicle.setYear(vehicleDto.getYear());
        vehicle.setVin(vehicleDto.getVin());
        vehicle.setLicensePlate(vehicleDto.getLicensePlate());
        vehicle.setVehicleType(VehicleType.valueOf(vehicleDto.getType().toUpperCase(Locale.ROOT)));
        vehicle.setModel(model);
    }
}
