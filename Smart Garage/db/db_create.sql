-- Дъмп на структурата на БД smart_garage
CREATE DATABASE IF NOT EXISTS `smart_garage` /*!40100 DEFAULT CHARACTER SET latin1 */;
USE `smart_garage`;

-- Дъмп структура за таблица smart_garage.manufacturers
CREATE TABLE IF NOT EXISTS `manufacturers`
(
    `manufacturer_id` bigint(20)  NOT NULL AUTO_INCREMENT,
    `name`            varchar(20) NOT NULL,
    PRIMARY KEY (`manufacturer_id`),
    UNIQUE KEY `manufacturers_name_uindex` (`name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 6
  DEFAULT CHARSET = latin1;


CREATE TABLE IF NOT EXISTS `models`
(
    `model_id`        bigint(20)  NOT NULL AUTO_INCREMENT,
    `manufacturer_id` bigint(20)  NOT NULL,
    `name`            varchar(20) NOT NULL,
    PRIMARY KEY (`model_id`),
    UNIQUE KEY `models_varchar_uindex` (`name`),
    KEY `models_manufacturer_fk` (`manufacturer_id`),
    CONSTRAINT `models_manufacturer_fk` FOREIGN KEY (`manufacturer_id`) REFERENCES `manufacturers` (`manufacturer_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 14
  DEFAULT CHARSET = latin1;


CREATE TABLE IF NOT EXISTS `roles`
(
    `role_id` bigint(20)  NOT NULL AUTO_INCREMENT,
    `name`    varchar(20) NOT NULL,
    PRIMARY KEY (`role_id`),
    UNIQUE KEY `role_name_uindex` (`name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 3
  DEFAULT CHARSET = latin1;


CREATE TABLE IF NOT EXISTS `services`
(
    `service_id` bigint(20)  NOT NULL AUTO_INCREMENT,
    `name`       varchar(50) NOT NULL,
    `price`      double      NOT NULL,
    PRIMARY KEY (`service_id`),
    UNIQUE KEY `services_name_uindex` (`name`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 14
  DEFAULT CHARSET = latin1;


CREATE TABLE IF NOT EXISTS `users_credentials`
(
    `users_credentials_id` bigint(20)   NOT NULL AUTO_INCREMENT,
    `email`                varchar(30)  NOT NULL,
    `password`             varchar(255) NOT NULL,
    `reset_password_token` varchar(45) DEFAULT NULL,
    PRIMARY KEY (`users_credentials_id`),
    UNIQUE KEY `users_credentials_email_uindex` (`email`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 53
  DEFAULT CHARSET = latin1;


CREATE TABLE IF NOT EXISTS `users_roles`
(
    `user_id` bigint(20) NOT NULL,
    `role_id` bigint(20) NOT NULL,
    KEY `users_roles_roles_fk` (`role_id`),
    KEY `users_roles_users_fk` (`user_id`),
    CONSTRAINT `users_roles_roles_fk` FOREIGN KEY (`role_id`) REFERENCES `roles` (`role_id`),
    CONSTRAINT `users_roles_users_fk` FOREIGN KEY (`user_id`) REFERENCES `user_details` (`user_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;


CREATE TABLE IF NOT EXISTS `user_details`
(
    `user_id`              bigint(20)  NOT NULL AUTO_INCREMENT,
    `first_name`           varchar(20) NOT NULL,
    `last_name`            varchar(20) NOT NULL,
    `phone`                varchar(13) NOT NULL,
    `users_credentials_id` bigint(20)  NOT NULL,
    PRIMARY KEY (`user_id`),
    KEY `users_users_credentials_fk` (`users_credentials_id`),
    CONSTRAINT `users_users_credentials_fk` FOREIGN KEY (`users_credentials_id`) REFERENCES `users_credentials` (`users_credentials_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 48
  DEFAULT CHARSET = latin1;


CREATE TABLE IF NOT EXISTS `vehicles`
(
    `vehicle_id`    bigint(20)  NOT NULL AUTO_INCREMENT,
    `license_plate` varchar(10) NOT NULL,
    `year`          bigint(20)  NOT NULL,
    `vin`           varchar(17) NOT NULL,
    `model_id`      bigint(20)  NOT NULL,
    `user_id`       bigint(20) DEFAULT NULL,
    `vehicle_type`  varchar(20) NOT NULL,
    PRIMARY KEY (`vehicle_id`),
    UNIQUE KEY `vehicles2_license plate_uindex` (`license_plate`),
    UNIQUE KEY `vehicles2_vin_uindex` (`vin`),
    KEY `vehicles_models_fk` (`model_id`),
    KEY `vehicle_users_fk` (`user_id`),
    CONSTRAINT `vehicle_users_fk` FOREIGN KEY (`user_id`) REFERENCES `user_details` (`user_id`),
    CONSTRAINT `vehicles_models_fk` FOREIGN KEY (`model_id`) REFERENCES `models` (`model_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 45
  DEFAULT CHARSET = latin1;


CREATE TABLE IF NOT EXISTS `visits`
(
    `visit_id`    bigint(20)  NOT NULL AUTO_INCREMENT,
    `visit_date`  date        NOT NULL,
    `vehicle_id`  bigint(20)  NOT NULL,
    `total_price` double      NOT NULL,
    `status_type` varchar(20) NOT NULL,
    PRIMARY KEY (`visit_id`),
    KEY `visits_vehicles_fk` (`vehicle_id`),
    CONSTRAINT `visits_vehicles_fk` FOREIGN KEY (`vehicle_id`) REFERENCES `vehicles` (`vehicle_id`)
) ENGINE = InnoDB
  AUTO_INCREMENT = 7
  DEFAULT CHARSET = latin1;


CREATE TABLE IF NOT EXISTS `visits_services`
(
    `visit_id`   bigint(20) NOT NULL,
    `service_id` bigint(20) NOT NULL,
    KEY `visits_services_services_fk` (`service_id`),
    KEY `visits_services_visits_fk` (`visit_id`),
    CONSTRAINT `visits_services_services_fk` FOREIGN KEY (`service_id`) REFERENCES `services` (`service_id`),
    CONSTRAINT `visits_services_visits_fk` FOREIGN KEY (`visit_id`) REFERENCES `visits` (`visit_id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;


