-- Дъмп данни за таблица smart_garage.manufacturers: ~5 rows (приблизително)
/*!40000 ALTER TABLE `manufacturers`
    DISABLE KEYS */;
INSERT IGNORE INTO `manufacturers` (`manufacturer_id`, `name`)
VALUES (1, 'Audi'),
       (2, 'BMW'),
       (3, 'Ferrari'),
       (4, 'Mercedes'),
       (5, 'Porsche');
/*!40000 ALTER TABLE `manufacturers`
    ENABLE KEYS */;

-- Дъмп данни за таблица smart_garage.models: ~12 rows (приблизително)
/*!40000 ALTER TABLE `models`
    DISABLE KEYS */;
INSERT IGNORE INTO `models` (`model_id`, `manufacturer_id`, `name`)
VALUES (1, 1, 'R8'),
       (2, 2, 'X5'),
       (3, 2, 'E90'),
       (4, 1, 'Q7'),
       (5, 3, 'F8 Spider'),
       (6, 5, 'Carrera'),
       (7, 4, 'S63'),
       (8, 1, 'A6'),
       (10, 1, 'RS6'),
       (11, 2, 'E60'),
       (12, 2, 'X6'),
       (13, 4, 'E-class W211');
/*!40000 ALTER TABLE `models`
    ENABLE KEYS */;

-- Дъмп данни за таблица smart_garage.roles: ~2 rows (приблизително)
/*!40000 ALTER TABLE `roles`
    DISABLE KEYS */;
INSERT IGNORE INTO `roles` (`role_id`, `name`)
VALUES (1, 'ROLE_ADMINISTRATOR'),
       (2, 'ROLE_CLIENT');
/*!40000 ALTER TABLE `roles`
    ENABLE KEYS */;

-- Дъмп данни за таблица smart_garage.services: ~10 rows (приблизително)
/*!40000 ALTER TABLE `services`
    DISABLE KEYS */;
INSERT IGNORE INTO `services` (`service_id`, `name`, `price`)
VALUES (2, 'Check Tires', 10.26),
       (3, 'Oil Change', 75),
       (4, 'Brake Repair and Replacement', 300),
       (7, 'Electrical Diagnostics', 50),
       (8, 'Wheel Alignment', 25),
       (9, 'Tune Up', 60),
       (10, 'Fuel System Repair', 89.8),
       (11, 'Exhaust System Repair', 97.2),
       (12, 'Air Conditioning A/C Repair', 55.3),
       (13, 'Engine Cooling System Maintenance', 150.7);
/*!40000 ALTER TABLE `services`
    ENABLE KEYS */;

-- Дъмп данни за таблица smart_garage.users_credentials: ~7 rows (приблизително)
/*!40000 ALTER TABLE `users_credentials`
    DISABLE KEYS */;
INSERT IGNORE INTO `users_credentials` (`users_credentials_id`, `email`, `password`, `reset_password_token`)
VALUES (1, 'ivayloanggelov@gmail.com', '$2a$10$LasQxgKgsDoS0yTZuQ4DIeKLsY50dWS/AFkRF3XAFpGdMXf.8vMwi', NULL),
       (2, 'anggelov1@abv.bg', '$2a$10$Ei.SaSJuA3nc0Q7yYWcE2Oki76uD4enduURdacAHxvoKM81TiLKme', NULL),
       (35, 'kirilgeorgiev@gmail.com', '$2a$10$IGD/84V/E.4u5/.0E3SF2O1mYzYm8qMGw6xO09prS13hnzQSBqdTq', NULL),
       (42, 'john.smith@gmail.com', '$2a$10$ZsrXb5H9MH4oGx2sur6n2e40oqux0PVDZ2mYW8ZJJEMR0Gbs8nOE6', NULL),
       (49, 'd.ivanov@gmail.com', '$2a$10$raR0BW.f0iCSLmRLa9QnwupmaxBhBIhWxdluiPfsapFNVxaeFLnTa', NULL),
       (51, 'atanas_tankov@gmail.com', '$2a$10$BjZiuC3m./eCOruOSIpyReSfqpzy9N0aP34UJIaElg85/5OdONM8S', NULL),
       (52, 'p.penev782@gmail.com', '$2a$10$VG1B6Bk8EkLK7CBv2AxPNusK/Qcr3rD/0md2Wg6x3/ZwSNw.dW8ye', NULL);
/*!40000 ALTER TABLE `users_credentials`
    ENABLE KEYS */;

-- Дъмп данни за таблица smart_garage.users_roles: ~7 rows (приблизително)
/*!40000 ALTER TABLE `users_roles`
    DISABLE KEYS */;
INSERT IGNORE INTO `users_roles` (`user_id`, `role_id`)
VALUES (1, 1),
       (2, 2),
       (30, 1),
       (37, 2),
       (44, 2),
       (46, 2),
       (47, 2);
/*!40000 ALTER TABLE `users_roles`
    ENABLE KEYS */;

-- Дъмп данни за таблица smart_garage.user_details: ~7 rows (приблизително)
/*!40000 ALTER TABLE `user_details`
    DISABLE KEYS */;
INSERT IGNORE INTO `user_details` (`user_id`, `first_name`, `last_name`, `phone`, `users_credentials_id`)
VALUES (1, 'Ivaylo', 'Angelov', '0876127150', 1),
       (2, 'Atanas', 'Angelov', '+359876127140', 2),
       (30, 'Kiril', 'Geoegiev', '+359887334150', 35),
       (37, 'John', 'Smith', '+44876127140', 42),
       (44, 'David', 'Ivanov', '+44876127141', 49),
       (46, 'Atanas', 'Tankov', '+44876127112', 51),
       (47, 'Pavel', 'Penev', '+359876127145', 52);
/*!40000 ALTER TABLE `user_details`
    ENABLE KEYS */;

-- Дъмп данни за таблица smart_garage.vehicles: ~7 rows (приблизително)
/*!40000 ALTER TABLE `vehicles`
    DISABLE KEYS */;
INSERT IGNORE INTO `vehicles` (`vehicle_id`, `license_plate`, `year`, `vin`, `model_id`, `user_id`, `vehicle_type`)
VALUES (38, 'BP8073CS', 2017, 'MNB12MN98H123HJNA', 1, 2, 'CAR'),
       (39, 'CB1337BP', 2019, 'JKI12MN98H123HJAA', 6, 37, 'CAR'),
       (40, 'TX6060CA', 2010, 'TYU12MN98H123HJNK', 3, 44, 'CAR'),
       (41, 'BP8073CA', 2005, 'MGH12ZN98H123POIU', 8, 2, 'CAR'),
       (42, 'M5060CB', 2012, 'MGH12ZN98H123POIB', 3, 46, 'CAR'),
       (43, 'A9009CA', 2012, 'ZXC12MN98H123HJNA', 12, 47, 'CAR'),
       (44, 'A1009CA', 2003, 'GHI12MN98H123HJAA', 13, 47, 'CAR');
/*!40000 ALTER TABLE `vehicles`
    ENABLE KEYS */;

-- Дъмп данни за таблица smart_garage.visits: ~6 rows (приблизително)
/*!40000 ALTER TABLE `visits`
    DISABLE KEYS */;
INSERT IGNORE INTO `visits` (`visit_id`, `visit_date`, `vehicle_id`, `total_price`, `status_type`)
VALUES (1, '2021-09-17', 38, 913.26, 'FINISHED'),
       (2, '2021-09-18', 44, 464.8, 'PREPARING'),
       (3, '2021-09-18', 38, 913.26, 'PREPARING'),
       (4, '2021-09-18', 42, 385.26, 'PREPARING'),
       (5, '2021-09-18', 43, 756.06, 'PREPARING'),
       (6, '2021-09-18', 41, 350, 'PREPARING');
/*!40000 ALTER TABLE `visits`
    ENABLE KEYS */;

-- Дъмп данни за таблица smart_garage.visits_services: ~37 rows (приблизително)
/*!40000 ALTER TABLE `visits_services`
    DISABLE KEYS */;
INSERT IGNORE INTO `visits_services` (`visit_id`, `service_id`)
VALUES (1, 3),
       (1, 12),
       (1, 11),
       (1, 13),
       (1, 8),
       (1, 10),
       (1, 2),
       (1, 7),
       (1, 9),
       (1, 4),
       (2, 7),
       (2, 10),
       (2, 4),
       (2, 8),
       (3, 3),
       (3, 4),
       (3, 2),
       (3, 13),
       (3, 8),
       (3, 11),
       (3, 10),
       (3, 12),
       (3, 9),
       (3, 7),
       (4, 4),
       (4, 3),
       (4, 2),
       (5, 13),
       (5, 2),
       (5, 12),
       (5, 7),
       (5, 8),
       (5, 3),
       (5, 4),
       (5, 10),
       (6, 7),
       (6, 4);
/*!40000 ALTER TABLE `visits_services`
    ENABLE KEYS */;